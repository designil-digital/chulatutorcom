<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package chulatutorcom
 */

get_header(); ?>

<div class="search--container">
	<div class="row">
		<div class="columns medium-12">
			
			<div class="search--container__result">
				<?php
				if ( have_posts() ) : ?>

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

				endwhile;

				// the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>
			</div>

			<?php
				wp_paginate();
			?>
				
		</div>
	</div>
</div>

	<?php
	// get_sidebar();
	get_footer();
