<?php 
get_header();
?>

	<?
if ( have_posts() ):
  while ( have_posts() ): the_post();

    $args = array(
      'post_parent' => get_the_ID(),
      'post_type'   => 'course', 
      'numberposts' => -1,
      );
    $child = get_children( $args );
?>

<?php
  if ( !empty($child) ) :
?>


  <?php
    if ( get_field('course_slider') ) :     
  ?>
  <div class="course-slide">
    <div class="row expanded"><a class="call show-for-small-only" href="tel:02-252-8633"></a>
      <div class="slide-group">
        <?php        
          $course_slider = get_field('course_slider');  
          if ( $course_slider ) :
            foreach ($course_slider as $key => $val) :
        ?> 
        <div class="item" style="background-image: url(<?php echo $val["image"]; ?>);">
          <div class="img column large-7 large-push-5"></div>
          <div class="say column large-5 large-pull-7">
            <div class="small-10 small-centered large-7 large-offset-4">
              <div class="title"><?php echo $val["title"]; ?></div>
              <?php echo $val["description"]; ?>
            </div>
          </div>
        </div>
        <?php
            endforeach;
          endif;
        ?>

      </div>
    </div>
  </div>
  <?php
    endif;
  ?>

  <?php
    if ( get_field('course_detail') ) :     
  ?>
  <div class="course-intro">
    <div class="row">
      <div class="medium-6 medium-push-6 column">
        <div class="video">
          <a class="youtube-popup" href="<?php the_field('course_video'); ?>">
            <img src="<?php the_field('course_image'); ?>">
          </a>
        </div>
      </div>
      <div class="medium-6 medium-pull-6 column">
        <div class="desc">
          <?php the_field('course_detail'); ?>
          <div class="price"><?php the_field('course_discount_price'); ?> บาท<span><?php the_field('course_price'); ?> บาท</span></div>
          <div class="time"><?php the_field('course_time'); ?></div>
        </div>
      </div>
    </div>
  </div>
  <?php
    endif;
  ?>

  <div class="course-detail">
    <div class="tabbar">
      <?php
        if ( get_field('course_notice') ):
      ?>
      <div class="column tab-list">
        <div class="row"><a class="detail active" href="#">รายละเอียดคอร์ส</a><a class="ps" href="#">หมายเหตุ</a></div>
      </div>
      <?php
        endif;
      ?>
      <div class="row">
        <div class="tabbar-content column">
          <div class="content detail fix-height">
            <div class="row">
              <div class="small-12 medium-push-9 medium-3 column">
                  <?php
                    $child = get_children( $args );
                    global $post;

                    if ( !empty($child) ) :
                  ?>
                    <div class="sub-menu--container">
                  <?php
                      foreach ( $child as $key => $post) :
                        setup_postdata($post);
                  ?>
                      <label><?php the_title(); ?></label>
                  <?php
                        $args = array(
                          'post_parent' => get_the_ID(),
                          'post_type'   => 'course', 
                          'numberposts' => -1,
                          );
                        $child_of = get_children( $args );
                        if ( $child_of ) :

                          echo '<ul class="sub-menu--child">';
                          foreach ( $child_of as $key => $post) {
                            setup_postdata($post);
                            echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
                          }
                          echo '</ul>';

                        endif;
                      endforeach;
                  ?>
                    </div>
                  <?php
                    endif;

                    wp_reset_query();
                  ?>
                </div>
              <div class="small-12 medium-pull-3 medium-9 column">
                <?php the_content(); ?>          
                <a class="read-more show-for-small-only" href="#">อ่านต่อ</a>
              </div>              
            </div>
          </div>
          <div class="content ps">          
            <?php the_field('course_notice'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- <div class="schedule">
    <div class="row">
      <div class="column">
        <h3>ตารางเรียน</h3>
      </div>
      <div class="column">
        <table class="hover unstriped" cellspacing="0">
          <thead>
            <tr>
              <th>รหัสคอร์ส</th>
              <th>วันเรียน</th>
              <th>รอบ</th>
              <th>เวลา</th>
              <th class="show-for-large">Test Date Exam</th>
              <th>เหลือที่นั่ง</th>
              <th class="show-for-large">อาจารย์</th>
              <th>สมัคร</th>
            </tr>
          </thead>
          <tbody>        
            <?php        
              $course_table = get_field('course_table');  
              if ( $course_table ) :
                foreach ($course_table as $key => $val) :
            ?> 
            <tr>
              <td><?php echo $val["code"]; ?></td>
              <td class="hilight"><?php echo $val["date"]; ?></td>
              <td><?php echo $val["round"]; ?></td>
              <td class="hilight"><?php echo $val["time"]; ?></td>
              <td class="show-for-large"><?php echo $val["test_date_exam"]; ?></td>
              <td class="hilight"><?php echo $val["available_seat"]; ?></td>
              <td class="show-for-large"><?php echo $val["instructor"]; ?></td>
              <td><a class="apply" href="<?php echo $val["link"]; ?>">สมัคร</a></td>
            </tr>
            <?php
                endforeach;
              endif;
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div> -->

  <div class="search-box clearfix">
    <div class="brand medium-12 large-6 column">
      <div class="lead-img small-3 large-5 column"><img src="<?php echo get_template_directory_uri(); ?>/img/brand/brand.png"></div>
      <div class="small-9 large-7 column">
        <p><?php the_field('trophy_title', 'option'); ?></p><span><?php the_field('trophy_by', 'option'); ?></span>
      </div>
    </div>
    <div class="search--form large-6 show-for-large column">

      <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
        <div class="large-6 column">
          <input type="text" name="s" id="s" placeholder="ค้นหาคอร์สเรียน">
          <input type="hidden" name="filter" id="filter" value="course">
        </div>
        <div class="large-6 column">
          <button>ค้นหาคอร์ส</button>
        </div>
      </form>

    </div>
  </div>
  <div class="course-ads">
    <div class="row expanded">
      <div class="img medium-6 medium-push-6 large-7 large-push-5 column"></div>
      <div class="apply medium-6 medium-pull-6 large-5 large-pull-7 column">
        <div class="small-10 small-centered medium-9 medium-offset-2 large-6 large-offset-5">
          <p><?php the_field('course_online_title'); ?></p>
          <span><?php the_field('course_online_description'); ?></span>
          <div class="clearfix"></div><a href="<?php the_field('course_online_link'); ?>">สมัครเรียน</a>
        </div>
      </div>
    </div>
  </div>
<?php
  else:
?>
  <div class="course-child--container">
    <div class="row">
      <div class="columns medium-9">

        <?php          
          $child = get_post_ancestors( get_the_ID() );          
          if ( !empty($child) ) :
            $parent_id = $child[count($child)-1];
        ?>

        <div class="breadcrumb">
          <div class="row">
            <div class="small-12 column">
              <ul>
                <li><a href="<?php echo get_the_permalink($parent_id); ?>"><?php echo get_the_title($parent_id); ?></a></li>
                <li><?php the_title(); ?></li>
              </ul>
            </div>
          </div>
        </div>

        <?php
          endif;
        ?>

        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
      </div>
      <div class="columns medium-3">
        <?php

          global $post;

          if ( !empty($child) ) :
            $parent_id = $child[count($child)-1];
            $args = array(
              'post_parent' => $parent_id,
              'post_type'   => 'course', 
              'numberposts' => -1,
              );
            $child = get_children( $args );
        ?>
          <div class="sub-menu--container">
        <?php
            foreach ( $child as $key => $post) :
              setup_postdata($post);
        ?>
            <label><?php the_title(); ?></label>
        <?php
              $args = array(
                'post_parent' => get_the_ID(),
                'post_type'   => 'course', 
                'numberposts' => -1,
                );
              $child_of = get_children( $args );
              if ( $child_of ) :

                echo '<ul class="sub-menu--child">';
                foreach ( $child_of as $key => $post) {
                  setup_postdata($post);
                  echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
                }
                echo '</ul>';

              endif;
            endforeach;
        ?>
          </div>
        <?php
          endif;
        ?>
        
      </div>
    </div>    
    <!-- <div class="schedule">
      <div class="row">
        <div class="column">
          <h3>ตารางเรียน</h3>
        </div>
        <div class="column">
          <table class="hover unstriped" cellspacing="0">
            <thead>
              <tr>
                <th>รหัสคอร์ส</th>
                <th>วันเรียน</th>
                <th>รอบ</th>
                <th>เวลา</th>
                <th class="show-for-large">Test Date Exam</th>
                <th>เหลือที่นั่ง</th>
                <th class="show-for-large">อาจารย์</th>
                <th>สมัคร</th>
              </tr>
            </thead>
            <tbody>        
              <?php        
                $course_table = get_field('course_table');  
                if ( $course_table ) :
                  foreach ($course_table as $key => $val) :
              ?> 
              <tr>
                <td><?php echo $val["code"]; ?></td>
                <td class="hilight"><?php echo $val["date"]; ?></td>
                <td><?php echo $val["round"]; ?></td>
                <td class="hilight"><?php echo $val["time"]; ?></td>
                <td class="show-for-large"><?php echo $val["test_date_exam"]; ?></td>
                <td class="hilight"><?php echo $val["available_seat"]; ?></td>
                <td class="show-for-large"><?php echo $val["instructor"]; ?></td>
                <td><a class="apply" href="<?php echo $val["link"]; ?>">สมัคร</a></td>
              </tr>
              <?php
                  endforeach;
                endif;
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div> -->
  </div>
<?php
  endif;
?>

<?php
  endwhile;
endif;
get_footer();
?>