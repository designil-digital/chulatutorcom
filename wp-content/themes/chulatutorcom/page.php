<?php 
get_header();
if ( have_posts() ):
  while ( have_posts() ): the_post();
?>
<div class="single-page">
  <?php
    if ( !isset($_GET["q"]) ):
  ?>
  <div class="lead-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>"></div>
  <?php
    endif;
  ?>
  <div class="content">
    <div class="row">
      <div class="medium-10 medium-centered large-12 column small-uncentered">
        <!-- <div class="breadcrumb">
          <ul>
            <li> <a href="#">Home</a></li>
            <li> <a href="#">Blog</a></li>
          </ul>
        </div> -->
      </div>
    </div>
    <div class="row">
      <div class="page-title">
        <div class="medium-10 medium-centered large-12">
          <div class="large-9 column">
            <h1><?php the_title(); ?></h1>
            <?php
              if ( !isset($_GET["q"]) ):
            ?>
            <span class="date"><?php the_date('d M Y'); ?></span>
            <?php
              endif;
            ?>
          </div>
          <div class="large-3 column">
            <div class="share-group">
              <div class="share-number"><?php echo get_facebook_count(get_the_permalink()); ?><br>SHARE</div>
              <div class="share-btn">
                <a class="fb" href="<?php the_permalink(); ?>">SHARE</a>
                <a class="tw" href="<?php the_permalink(); ?>">TWEET</a>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="medium-10 medium-centered large-8 large-uncentered column">
        <?php the_content(); ?>
      </div>
      <div class="medium-12 large-4 column">
        <div class="medium-10 medium-centered">
          <div class="sidebar">
            <!-- <div class="widget"><img src="<?php echo get_template_directory_uri(); ?>/img/single/exam1.jpg"></div> -->
            <div class="widget">
              <div class="fb-page" data-href="https://www.facebook.com/chulatutor.co.th/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/chulatutor.co.th/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/chulatutor.co.th/">Chulatutorbyple</a></blockquote></div>
              <div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=502368376628948";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));</script>
            </div>
            <div class="widget">
              <div class="follow">
                <p>FOLLOW CHULAR TUTOR</p>                
                <?php  
                  $fb = get_field('facebook', 'option');
                  $tt = get_field('twitter', 'option');
                  $gg = get_field('google_plus', 'option');
                  $yt = get_field('youtube', 'option');

                  if ( $fb ):
                ?>                
                <a class="fb" href="<?php echo $fb; ?>"></a>
                <?php
                  endif;
                  if ( $tt ):
                ?>
                <a class="tw" href="<?php echo $tt; ?>"></a>
                <?php
                  endif;
                  if ( $gg ):
                ?>
                <a class="gp" href="<?php echo $gg; ?>"></a>
                <?php
                  endif;
                  if ( $yt ):
                ?>
                <a class="yt" href="<?php echo $yt; ?>"></a>
                <?php
                  endif;
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
  endwhile;
endif;
get_footer();
?>