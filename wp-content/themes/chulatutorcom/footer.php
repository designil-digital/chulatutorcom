<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chulatutorcom
 */

?>

<?php
  if ( !is_page('about-us') && !is_singular() ) :
?>
<div class="footer social">
  <div class="row">
    <div class="small-10 small-centered medium-12 medium-centered column">
      <h3>Follow us on</h3>
      <?php
        $fb = get_field('facebook', 'option');
        $tt = get_field('twitter', 'option');
        $ig = get_field('instagram', 'option');
        $ln = get_field('line', 'option');
        $yt = get_field('youtube', 'option');
        $gg = get_field('google_plus', 'option');

        if ( $fb ):
      ?>
        <a href="<?php echo $fb; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/fb.png"></a>
      <?php
        endif;
        if ( $tt ):
      ?>
      <a href="<?php echo $tt; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/tw.png"></a>
      <?php
        endif;
        if ( $ig ):
      ?>
      <a href="<?php echo $ig; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/ig.png"></a>
      <?php
        endif;
        if ( $ln ):
      ?>
      <a href="<?php echo $ln; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/ln.png"></a>
      <?php
        endif;
        if ( $yt ):
      ?>
      <a href="<?php echo $yt; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/yt.png"></a>
      <?php
        endif;
        if ( $gg ):
      ?>
      <a href="<?php echo $gg; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/gp.png"></a>
      <?php 
        endif;
      ?>
    </div>
  </div>
</div>
<?php
  endif;
?>

<?php  
  if ( !is_page('about-us') && !is_page('tutor-register') && !is_singular() && !is_search() ) :
?>
<div class="footer quote">
  <div class="row">
    <div class="small-10 small-centered medium-6 medium-centered column"><?php the_field('footer_quote', 'option'); ?></div>
  </div>
</div>
<?php
  endif;
?>

<?php
  if ( is_front_page() || is_home() && !is_singular() ) :
?>
<div class="footer about show-for-medium">
  <div class="row">
    <div class="medium-9 medium-centered column">
      <div class="content fix-height">
        <?php the_field('footer_introduction', 'option'); ?>
      </div>
      <div class="read-more"><a htef="#">อ่านต่อ</a></div>
    </div>
  </div>
</div>
<?php
  endif;
?>

<?php
  if ( !is_page('about-us') && !is_page('tutor-register') && !is_search() ) :
?>
<div class="footer course">
  <div class="row">
    <div class="medium-9 medium-centered column">
      <h3>คอร์สเรียน</h3>
    </div>
    <div class="medium-9 medium-centered column">
      <div class="row">
        <?php
          $course = get_field('footer_course', 'option'); 
          if ( $course ) :
            foreach ($course as $key => $val) :
        ?>
        <div class="item medium-6 large-4 column clearfix">
          <div class="icon float-left"><img src="<?php echo $val["image"]; ?>"></div>
          <div class="desc float-left">
            <div class="title"><?php echo $val["title"]; ?></div>
            <p><?php echo $val["description"]; ?></p><a href="<?php echo $val["link"]; ?>" target="_blank">สมัครเรียน ></a>
          </div>
        </div>
        <?php
            endforeach;
          endif;
        ?>
      </div>
    </div>
  </div>
</div>
<?php
  endif;
?>

<?php
  if ( !is_page('tutor-register') && !is_search() ) :
?>
<div class="footer course-link show-for-medium">
  <div class="row">
    <div class="medium-9 medium-centered column">
      <h3>คอร์สเรียน</h3>
      <ul>
        <?php
          $tag = get_field('tag', 'option');
          if ( $tag ) :
            foreach ($tag as $key => $val) :
        ?>
        <li><a href="<?php echo get_term_link($val->term_id); ?>"><?php echo $val->name; ?></a></li>
        <?php                    
            endforeach;
          endif;
        ?>
      </ul>
    </div>
  </div>
</div>
<?php
  endif;
?>

<footer>
  <div class="foot-nav">
    <div class="row">
      <div class="medium-9 medium-centered column">
        <div class="row">
          <div class="medium-4 large-3 column show-for-medium">
            <?php 
              $theme_locations = get_nav_menu_locations();    
              $menu_obj_1 = get_term( $theme_locations['footer-menu-1'], 'nav_menu' );
            ?>            
            <p class="title"><?php echo $menu_obj_1->name; ?></p>
            <?php
              $defaults = array( 
                  'theme_location' => 'footer-menu-1',
                  'container'       => '', 
                  'container_class' => '',
                  'menu_class'      => '',
                  'menu_id'         => '',
                  'items_wrap'      => '<ul class="footer-menu">%3$s</ul>',
                );
              wp_nav_menu($defaults); 
            ?>        
          </div>
          <div class="medium-2 large-2 column show-for-large">
            <?php 
              $menu_obj_2 = get_term( $theme_locations['footer-menu-2'], 'nav_menu' );
            ?>            
            <p class="title"><?php echo $menu_obj_2->name; ?></p>
            <?php
              $defaults = array( 
                  'theme_location' => 'footer-menu-2',
                  'container'       => '', 
                  'container_class' => '',
                  'menu_class'      => '',
                  'menu_id'         => '',
                  'items_wrap'      => '<ul>%3$s</ul>',
                );
              wp_nav_menu($defaults); 
            ?>  
          </div>
          <div class="medium-6 large-4 column">
            <?php
              $defaults = array( 
                  'theme_location' => 'footer-menu-3',
                  'container'       => '', 
                  'container_class' => '',
                  'menu_class'      => '',
                  'menu_id'         => '',
                  'items_wrap'      => '<ul class="footer-menu">%3$s</ul>',
                );
              wp_nav_menu($defaults); 
            ?>  
            <p class="address"><?php the_field('footer_address', 'option'); ?></p>
            <p class="mail"><?php the_field('footer_email', 'option'); ?></p>
            <div class="phone-group">
              <div class="phone"><?php the_field('footer_tel', 'option'); ?></div><a class="call show-for-small-only" href="tel:<?php the_field('footer_tel', 'option'); ?>">CLICK TO CALL</a>
            </div>
          </div>
          <div class="medium-2 large-3 column hide-for-small-only">
            <p class="title">โซเชี่ยล</p>
            <div class="social-btn">
              <?php
                $fb = get_field('facebook', 'option');
                $tt = get_field('twitter', 'option');
                $ig = get_field('instagram', 'option');
                $ln = get_field('line', 'option');
                $yt = get_field('youtube', 'option');
                $gg = get_field('google_plus', 'option');
                
                if ( $fb ):
              ?>
                <a href="<?php echo $fb; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/w_fb.png"></a>
              <?php
                endif;
                if ( $tt ):
              ?>
              <a href="<?php echo $tt; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/w_tw.png"></a>
              <?php
                endif;
                if ( $ig ):
              ?>
              <a href="<?php echo $ig; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/w_ig.png"></a>
              <?php
                endif;
                if ( $ln ):
              ?>
              <a href="<?php echo $ln; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/w_ln.png"></a>
              <?php
                endif;
                if ( $yt ):
              ?>
              <a href="<?php echo $yt; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/w_yt.png"></a>
              <?php
                endif;
                if ( $gg ):
              ?>
              <a href="<?php echo $gg; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/footer/w_gp.png"></a>
              <?php 
                endif;
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copy">
    <div class="row">
      <div class="column">
        <?php the_field('copyright', 'option'); ?>
        <a href="//www.iubenda.com/privacy-policy/7888106" class="iubenda-white no-brand iub-legal-only iubenda-embed" title="Privacy Policy">Privacy Policy</a>
        <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
        <style>
          iframe.iubenda-ibadge {
            display: inline-block;
            vertical-align: middle;
          }
        </style>
      </div>
    </div>
  </div>
</foooter>

<a href="#" class="go-to-top">
  <img src="<?php echo get_template_directory_uri(); ?>/img/nav/up-arrow.png">
</a>

<?php wp_footer(); ?>

</body>
</html>
