<?php session_start();
/*
* Template Name: reserve seat
*/

get_header();

?>
      <script src="../date-jscssfile/js/jquery.js"></script>
<script src="../date-jscssfile/js/jquery.datetimepicker.full.js"></script>
<script>
      
      $('#date_off').datetimepicker({
	
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	
});
      
      </script>   
<div class="tutor-register header">
  <h2>ค้นหาวันเรียนชดเชย</h2>
</div>
<div class="tutor-register intro">
  <div class="img medium-4 column">
    <div class="img1" style="background-image:url('<?php the_field('tutor_image_1', 'option'); ?>');"></div>
    <div class="img2 show-for-medium" style="background-image:url('<?php the_field('tutor_image_2', 'option'); ?>');"></div>
  </div>
  <div class="detail medium-8 large-7 column end">
  
  <article class="s-12 l-8 ">
        
        
  
<?


include("connect.php");

//echo "mod==".$_GET["mod"];
$dateReserve=date("Y-m-d");
if($_GET["mod"]=="res" or $_GET["mod"]=="cancel" ){
	
	//echo "get<br>";
	$dateSearch=$_GET["dateSearch"];
	$dateStudy=$_GET["datestudy"];
	$dayNo=$_GET["dayNo"];
	$sub_id=$_GET["sub_id"];
	$std_id=$_GET["std_id"];
	$typeID=$_GET["typeID"];
		 $std="select * from pf_students where std_id='$std_id'";
	 // echo "$std<br>";
	  $rsstd=mysql_query($std,$conn) or die ("ดูข้อมูลนักเรียนไม่ได้");
	  $dbstd=mysql_fetch_array($rsstd);
	  $std_fullname=$dbstd["std_fullname"];
	  $std_phone=$dbstd["std_phone"];
	  $std_email=$dbstd["std_email"];
	  
		$cos_id_study=$_GET["cos_id_s"];
		$cos_id_from=$_GET["cos_id_f"];
		$cos_id_chk=$cos_id_from;
		$date_off=$_GET['date_off'];
		$timeall=$_GET["timeall"];
		$t1=explode("-",$timeall);
		$time_start=$t1[0];
		$time_end=$t1[1];
		$dayNo=$_GET["dayNo"];
		$topic=$_GET["topic"];
		
		
		$datestr1=explode("-",$dateStudy);
					$d1=$datestr1[2];
					$m1=$datestr1[1];
					$y1=$datestr1[0];
					$dateStr="$d1-$m1-$y1";
		//$sqlp="select * from std_course_reserve where cos_id_from='$cos_id_chk' and topic='$topic' and std_id='$std_id'";
		$sqlp="select * from std_course_reserve where  dayNo='$dayNo' and std_id='$std_id'";
		//echo "$sqlp<br>";
		$rsp=mysql_query($sqlp) or die ("ดูข้อมูลการจองไม่ได้");
		if(mysql_num_rows($rsp)>0){
			
			$dbp=mysql_fetch_array($rsp);
			$countrowdel=$dbp["countrow"];
			$res_cos_id=$dbp["cos_id_study"];
			$status_reserve_done="yes";
			$rd1=explode("-",$dbp["dateStudy"]);
			$res_date=$rd1[2]."-".$rd1[1]."-".$rd1[0];
			
			
			
			if($_GET["mod"]=="res"){
			/* echo "<script>alert('คูณทำการจองที่นั่งสำหรับการเรียนชดเชยนี้แล้ว วันที่ $res_date ถ้าคุณทำการจองที่นั่งใหม่ ระบบจะยกเลิกการจองสำหรับที่นั่งเก่าอัติโนมัติ')</script>"; */
					
	
				$message="คุณทำการเปลี่ยนวันเรียนชดเชยเรื่อง $topic วันที่ $res_date เป็น วันที่ $dateStr ขอบคุณคะ";
				$mail_title="เปลี่ยนวันเรียนชดเชย[Chulatutor.com]";
				
				echo "<script>alert('คุณทำการเปลี่ยนวันเรียนชดเชยเรื่อง $topic วันที่ $res_date เป็น วันที่ $dateStr ขอบคุณคะ')</script>"; 
			
			}elseif($_GET["mod"]=="cancel"){
				
				$message="คุณทำการยกเลิกวันเรียนชดเชยเรื่อง $topic วันที่ $res_date ขอบคุณคะ";
				echo "<script>alert('คุณทำการยกเลิกวันเรียนชดเชยเรื่อง $topic วันที่ $res_date ขอบคุณคะ')</script>";
				$mail_title="ยกเลิกวันเรียนชดเชย[Chulatutor.com]";
				$status_reserve_done="no";
			
			}else{
				$status_reserve_done="no";
			} // end if get==res
			
			$del="delete from std_course_reserve where countrow='$countrowdel' ";
			//echo "$del<br>";
			$rsdel=mysql_query($del) or die ("ลบข้อมูลการจองไมได้");
		}else{ // พบข้อมูลเคยทำการจองแล้ว
			$message="คุณทำการจองวันเรียนชดเชยเรื่อง $topic วันที่ $dateStr ขอบคุณคะ";
			$mail_title="จองวันเรียนชดเชย[Chulatutor.com]";
			}
	
		if($_GET["mod"]=="res"){
			$dayNo=$_GET["dayNo"];
		$add="insert into std_course_reserve (typeID,std_id,cos_id_study,cos_id_from,dateStudy,topic,dayNo,time_start,time_end,sub_id,ApproveBy,dateReserve) values ('$typeID','$std_id','$cos_id_study','$cos_id_from','$dateStudy','$topic','$dayNo','$time_start','$time_end','$sub_id','$ApproveBy','$dateReserve')"; 
		//echo "$add<br>";
		$rsa2=mysql_query($add) or die ("เพิ่มข้อมูลการจองไมได้");
		echo "<script>alert('คุณได้จองที่นั่งเรียนชดเชยเรียบร้อยและคุณสามารถตรวจสอบรายละเอียดเรียนชดเชยได้ที่ อีเมล์ที่ใช้สมัครเรียน')</script>";
		
		$status_reserve_done="yes";
		} // end แก้ไขข้อมูลการจอง db
		
		if($_GET["mod"]=="res" or $_GET["mod"]=="cancel" ){
		
		///////// ส่งsms ///////////////////
		//include("send_sms_res.php");
		include("sms.class.php");
	/*$datestr1=explode("-",$dateStudy);
	$d1=$datestr1[2];
	$m1=$datestr1[1];
	$y1=$datestr1[0];
	$dateStr="$d1-$m1-$y1";*/
	
	//$message="คุณทำการจองวันเรียนชดเชยเรื่อง $topic วันที่ $dateStr ขอบคุณคะ";	
	//exit();
	$username = "chulatutor";//$_REQUEST['username'];
	$password = "2123";//$_REQUEST['password'];
	//$msisdn = "$sendtel";//$_REQUEST['msisdn']; เบอร์โทรที่จะส่ง
	//$message = $_REQUEST['message'];
	$sender = "CHULATUTOR";//$_REQUEST['sender'];
	$cc=date("ymdhi");
	$ScheduledDelivery = $cc;//$_REQUEST['ScheduledDelivery']; //* 1207011545 (ปีเดือนวันชั่วโมงนาที)
	$force = "premium";// $_REQUEST['force']; //standard/premium
	
	//$ctel=explode(",",$sendtel);
	$ctel=$std_phone; //echo "ctel==$ctel<br>";
	//$count=count($ctel);
	//for($i=0;$i<$count;$i++){
		
	$msisdn =$ctel; //echo "$i==$msisdn<br>";
	$result = sms::send_sms($username,$password,$msisdn,$message,$sender,$ScheduledDelivery,$force); 
	//echo $result."<br>";
	//} // end for
		
	///////// ส่งเมล์ ///////////////////	
	//include("send_email_res.php");
		
		function send_main($sender,$send_name,$send_to,$subject,$mess ){
	    //กำหนดชื่อ และ E-mail Address ของผู้รับ 
    $reci = $send_to; 
     
    //กำหนด Subject ของ E-mail 
    $subj = $subject; 
     
    //กำหนดเนื้อความของ E-mail 
    $mess = $mess ; 
   	$boundary = uniqid("");
    //กำหนดส่วนของ Header 
   //กำหนดส่วนของ Header 
	
	
    //กำหนดส่วนของ Header 
	$html = "<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n";
	$html .= "<meta name=\"Generator\" content=\"Microsoft SafeHTML\">\r\n";
	$html .= "<title>$mail_title</title><body>\r\n";
	$head = "Content-type: text/html; charset=utf-8\r\n";
    $head .= "From: $send_name <$sender>\r\n"; 
	$head .= "Reply-To: $send_name <$sender>"; 
	$head .= "bcc: BCC To Admin <admin@chulatutor.com>\n"; 
     $mess=$html.$mess."<body><html>";
     
	 
	 include("phpmailer/smtp-gmail.php");	
   //$flgSend= @mail( $_POST['email'], $subject_mail, $mess, $head ); 
		
		//เอาค่าที่เราตั้งไว้ทำการส่ง 
		smtpmailer($reci,'no-reply@chulatutor.com', 'chulatutor', $subj, $mess);
    
  /* $flgSend= @mail($reci, $subj, $mess, $head ); 
		if($flgSend)
	{
		//echo  "$reci [ Send ok]<br>";
	}
	else
	{
		echo "<scritp>alert('ไม่สามารถส่ง Email ได้')</script>";
		//echo "$reci [Can Not Send.]<br>";
	} */
}// end function

	$sender="admin@chulatutor.com";
	//$message="คุณทำการจองวันเรียนชดเชยเรื่อง $topic วันที่ $dateStr ขอบคุณคะ";
	$mess=$message;
	$send_name="Chulatutor";
	$subject="จองวันเรียนชดเชย [Chulatutor.com]";
	
	//echo "std_email==$std_email<br>";
		if($std_email<>""){
		//	$cm=explode(",",$std_email);
			//$count=count($cm);
			//for($i=0;$i<$count;$i++){
				$send_to=$std_email;
				send_main($sender,$send_name,$send_to,$subject,$mess );
		
			//}// end for
		} // end if std_email<>""
	} // end if ส่ง sms& mail ขั้นตอนการจองเรียนชดเชย
		
		$topic="";
		$dayNo="";
		
	$chkoff="select * from std_course_create_detail where sub_id='$sub_id' and dateStudy='$dateSearch' $sqlstrtime ";
	//echo "$chkoff<br>";
	$rschkoff=mysql_query($chkoff) or die("ดูข้อมูลวันที่ขาดเรียนไม่ได้");
	while($dboff=mysql_fetch_array($rschkoff)){
		
		if($topic==""){
			$topic=$dboff["topic"];
		}else{
			$topic=$topic.",".$dboff["topic"];
		}
		
		if($dayNo==""){
			$dayNo=$dboff["dayNo"];
		}else{
			$dayNo=$dayNo.",".$dboff["dayNo"];
		}
	
	} // end while หัวข้อที่ขาดวันนั้น	
	
	$topic_a=explode(",",$topic);
$count_topic=count($topic_a);
$dayNo_a=explode(",",$dayNo);
$count_dayNo=count($dayNo_a);
//echo "count==$count_topic//$count_dayNo<br>";
		
} // end if mod =res  or mod=cancel

/*function DateDiff($strDate1,$strDate2)
	 {
	return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
	 }
	 */
	// echo "passsssssss";
	 //exit();


 if($_SERVER["REQUEST_METHOD"] === "POST" and $_POST[cos_id]!="")
    {
        //form submitted

        //check if other form details are correct

        //verify captcha
      /*  $recaptcha_secret = "6LcIZg8TAAAAAKmX5NEJS4mDUUTgSvkeqVmmlX_J";
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$_POST['g-recaptcha-response']);
        $response = json_decode($response, true);
        if($response["success"] === true)
        {
            //echo "Logged In Successfully";
			$status_robot="no";
        }
        else
        {
            //echo "You are a robot";
			
			echo "<script>alert('คุณยังไม่ได้ยืนยันตนว่าไม่ใช่ spam')</script>";
			$status_robot="yes";
        }
    
	*/
	
	
	
	  if($_POST['code']==""){
	echo "<script>alert('คุณยังไม่ได้ระบุตัวเลขในการตรวจสอบว่าคุณไม่ใช่ spam');</script>";
		//exit()  ;
		$status_robot="yes";
	
	}else{

if(($_POST['code']) == $_SESSION['code']) { 
		$code = $_POST['code'];
		$status_robot="no";
		
		} else { 
 
		echo "<script>alert('คุณระบุตัวเลขยืนยันตัวตนว่าไม่ใช่ spam ไม่ถูกต้อง');</script>";
		//exit()  ;
		$status_robot="yes";
		}
  
	  if($_POST["std_phone"]<>"" and $status_robot=="no"){
		 // echo "postttt<br>";
  		$std_phone=$_POST["std_phone"];
	  $tel=$std_phone;
	  $searchPage='course_O';
	  //exit();
	  //include("chkcount.php");


}// if post !=""
	} // end ระบุ code แล้ว

}// end if post done

	  if($_POST["cos_id"]<>"" and $status_robot=="no"){
	  $cos_id_chk=$_POST["cos_id"];
	   $date_off=$_POST["date_off"];
	  $time_start=$_POST["timestart_off"];
	$time_end=$_POST["timeend_off"];
	  }
	  if($std_phone!=""){
	  $std="select * from pf_students where std_phone='$std_phone'";
	  //echo "$std<br>";
	  $rsstd=mysql_query($std,$conn) or die ("ดูข้อมูลนักเรียนไม่ได้");
	  $dbstd=mysql_fetch_array($rsstd);
	  $std_fullname=$dbstd["std_fullname"];
	  $std_id=$dbstd["std_id"];
	  }
	 
	  $df1=explode("-",$date_off);
	$date_off_str=$df1[2]."-".$df1[1]."-".$df1[0];

	
	/*if($time_start<>""){
		$sqlstrtime=" and time_start='$time_start'";

		}
	if($time_end<>""){
		$sqlstrtime=$sqlstrtime." and time_end='$time_end'";
		}	*/
	  
	  //exit();
	  
	  //echo "cos_id_chk==$cos_id_chk<br>";
	  if($cos_id_chk<>""){
	  ///// check ข้อมูลว่าลงทะเบียนคอสนี้สมบูรณืหรือเปล่า ///////////
	  //$reg="select * from pf_regis where (cos_id='$cos_id_chk' or move_cos_id='$cos_id_chk') and std_id='$std_id' and reg_status='อนุมัติ' and get_book='1'";
	  $reg="select * from pf_regis where (cos_id='$cos_id_chk' or move_cos_id='$cos_id_chk') and std_id='$std_id' and reg_status='อนุมัติ' and get_book='1'";
	 // echo "$reg<br>";
	  
	  $rsreg=mysql_query($reg,$conn) or die ("ดูข้อมูลการลงทะเบียนไม่ได้");
	  if(mysql_num_rows($rsreg)>0){
		
		  
		 }else{
			echo "<script>alert('ไม่พบข้อมูลการลงทะเบียน หรือการลงทะเบียนไม่สมบูรณ์ กรุณาติดต่อสถาบัน');window.location='../reserve_seat2/';</script>";
			
			exit();
			  
		}
		
		$mc="select * from pf_course where cos_id='$cos_id_chk'";
		//echo "$mc<br>";
		$rsmc=mysql_query($mc,$conn) or die ("ดูข้อมูลคอรสเรียนไม่ได้");
		$dbmc=mysql_fetch_array($rsmc);
		$std_course_typeID=$dbmc["std_course_typeID"];
		//echo $dbmc["cos_subject_type"];
		if($dbmc["cos_subject_type"]<>"MC"){
			echo "<script>alert('คอร์สที่คุณลงทะเบียน ไม่ใช่ MainCourse ไม่สามารถทำการจองที่นั่งเรียนชดเชยได้');</script>";
			//exit();
			
			}
			
		////// ตรวจสอบว่าคอสที่เรียน ยังเปิดคอร์สอยู่มั้ย //////////	

	  }




?>    
        
<!--- -->       
          <table width="100%" border="0" style="padding-bottom:5px;">
            <tr>
              <td background="https://chulatutor-chulatutor.netdna-ssl.com/images/icon_header.png" style="background-repeat:no-repeat; height:30px;"><div style="padding-left:15px; padding-top:10px; color:white; font-size:14px;"><strong>ค้นหาวันเรียนชดเชย</strong></div></td>
            </tr>
          </table>
          <hr>
          <div id="noform">
            
  <form id="form1" name="form1" method="post" action="">
  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="1" class="table table-bordered">
    <tr>
      <td width="25%"><strong>เบอร์โทรศัพท์*</strong></td>
      <td width="75%">
        <input name="std_phone" type="text" id="std_phone" value="<? echo $std_phone;?>" style="height:30px;" />
        <br />
  <input class='btn' type="submit" name="search" id="search" value="ค้นหาข้อมูลนักเรียน" />
        &lt;-- 1.กรอกข้อมูลเบอร์โทรเพื่อทำรายการขั้นต่อไป</td>
    </tr>
   
   <? if($std_id!=""){?>
    <tr>
      <td><strong>ชื่อนักเรียน*</strong></td>
      <td><? echo $std_fullname;?><input name="std_id" type="hidden" id="std_id" value="<? echo $std_id;?>" /></td>
    </tr>
    <tr>
      <td><strong>คอร์สที่เรียนอยู่*</strong></td>
      <td>
      
      <? 
	  
	  $today=date("Y-m-d");
$sql="select * from  pf_regis as pr,pf_course as pc where pr.cos_id=pc.cos_id and pr.reg_status='อนุมัติ' and pr.std_id='$std_id' and pc.cos_date_end>='$today' and pc.cos_subject_type='MC' ";
//echo "$sql<br>";
$rs=mysql_query($sql); 

?>
      <select name='cos_id' id='cos_id' onchange="get_listcos_id(this.value);" >
	<option value="no">เลือกคอร์สที่คุณเรียน</option>
          <?


	while ($row=mysql_fetch_array($rs)){
		$i=$i+1;
		
		if($cos_id_chk==$row['cos_id']){
			$select="selected";
			}else{
				$select="";
				}
			   echo "<option value=".$row['cos_id']." $select> $i. ".$row['cos_id']."</option>";
		
	 }
?>
    </select></td>
    </tr>
    <tr>
      <td><strong>วันที่ขาดเรียน*</strong></td>
      <td><input name='date_off'  type='text'   id='date_off' value="<? echo $date_off; ?>" size="10" style="height:30px;">
      

      </td>
    </tr>
    <tr>
      <td><span class="sat"><img src="../captcha/CaptchaSecurityImages.php?width=100&amp;height=40&amp;characters=4" /></span></td>
      <td><span class="sat">
        <input type="text" name="code" style="height:30px;" />
Number anti spam. *</span>
        <input class='btn' type="submit" name="ok" id="ok" value="ค้นหาข้อมูลวันเรียนชดเชย" />
        &lt;-- 2.ค้นหาจากข้อมูลที่คุณระบุด้านบน</td>
    </tr>
    
    <? } //end std_phone<>''?>
  </table>
</form>


</div> <!-- end form contain -->
  


<!--- -->
  <!-- end contain -->   
<hr />

  <div class="clearfix"></div>
  <div class="clearfix"></div>
  <? 
  
  //echo $_POST["date_off"]."/".$_POST["std_phone"]."/".$_POST["cos_id"];
  
 // echo "$std_phone/$cos_id_chk/$date_off<br>";
  if($std_phone!="" and $cos_id_chk!="" and $date_off!=""){?>         	
   	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
       <h2> รายการวันเรียนชดเชยที่คุณสามารถทำการจองที่นั่งเรียนได้ </h2>
          <div class="table-responsive" style="width:100%;">
            
        <? 
	  
	

	  
	//$cos_id_chk=$_POST["cos_id"];
	 ///ตรวจสอบรายการคอสที่อยู่ในเงื่อนไขการเรียนเดียวกัน /////
	 
	 $chkcos="select * from pf_course where cos_id='$cos_id_chk'";
	 //echo "$chkcos<br>";
	 $rschk=mysql_query ($chkcos) or die ("ดูข้อมูลวิชาไม่ได้");
	 $dbc=mysql_fetch_array($rschk);
	/////// เตรียมข้อมูลเช็คตามเงื่อนไข ////////
	 $cosnow_subject_type=$dbc["cos_subject_type"]; //หาประเภทของวิชาที่เรียนว่าเป็น mc มั้ย
	 $cosnow_enddate=$dbc["cos_date_end"];// หาวันสุดท้ายของคอร์สที่เรียนปัจจุบัน
	 //$cosnow_enddate="2015-11-14";
	$datenow=date("Y-m-d"); //echo "$datenow/$cosnow_enddate<br>";
	
	if(DateDiff("$datenow","$cosnow_enddate")>0){
		//echo "ยังไม่หมดเวลาแล้ว";
		
		}else{
			echo "<script>alert('ขออภัย คุณไม่สามารถจองที่นั่งเรียนชดเชยได้ เนื่องจากคอร์สที่คุณลงเรียนปิดคอร์สแล้ว')</script>";
			//echo "หมดเวลาแล้ว";
			exit();
			
			}


	 $sub_id=$dbc["sub_id"];
	 $sub="select * from pf_subject where sub_id='$sub_id'";
	 //echo "$sub<br>";
	 $rssub1=mysql_query($sub) or die ("ดูข้อมูลวิชาไม่ได้");
	 $dbs=mysql_fetch_array($rssub1);
	 $sub_name1=$dbs["sub_name"];
  	$std_course_typeID=$dbc["std_course_typeID"];
	
		
		if($_GET["dateSearch"]<>""){
			$date_off=$_GET["dateSearch"];
			$topic="";
		}
		
	$chkoff="select * from std_course_create_detail where sub_id='$sub_id' and dateStudy='$date_off' $sqlstrtime ";
	//echo "$chkoff<br>";
	$rschkoff=mysql_query($chkoff) or die("ดูข้อมูลวันที่ขาดเรียนไม่ได้");
	while($dboff=mysql_fetch_array($rschkoff)){
		
		if($topic==""){
			$topic=$dboff["topic"];
		}else{
			$topic=$topic.",".$dboff["topic"];
		}
		
		if($dayNo==""){
			$dayNo=$dboff["dayNo"];
		}else{
			$dayNo=$dayNo.",".$dboff["dayNo"];
		}
	
	} // end while หัวข้อที่ขาดวันนั้น
	echo "<div style=\"font-size:14px;\">วันที่ <strong style=\"color:#FF3399\">$date_off_str </strong>คุณขาดเรียนวิชา <strong>$sub_name1 </strong> เรื่อง  <strong style=\"color:#FF6600\">$topic</strong> สามารถชดเชยได้วันที่</div>";
	

$topic_a=explode(",",$topic);
$count_topic=count($topic_a);//echo "count==$count_topic<br>";
$dayNo_a=explode(",",$dayNo);
$count_dayNo=count($dayNo_a);

for($i=0;$i<$count_topic;$i++){	

$topic=$topic_a[$i];
$dayNo=$dayNo_a[$i];//echo "$topic//$dayNo<br>";

if($str_alldayNo==""){
	$first_dayNo=$dayNo_a[$i];
	$str_alldayNo="dayNo='$first_dayNo' ";
	}else{
		$str_alldayNo=$str_alldayNo." or dayNo='$dayNo'";
		}
//echo "topic==$topic<br>";
	// ตรวจสอบว่าเคยจองที่นั่งเรียนชดเชยหรือยัง ///////	
		//$sqlp="select * from std_course_reserve where sub_id='sub_id' and std_id='$std_id' and topic='$topic'";
		$sqlp="select * from std_course_reserve where sub_id='sub_id' and std_id='$std_id' and dayNo='$dayNo'";
		//echo "$sqlp<br>";
		$rsp=mysql_query($sqlp) or die ("ดูข้อมูลการจองไม่ได้");
		if(mysql_num_rows($rsp)>0){
			
			$dbp=mysql_fetch_array($rsp);
			$countrowdel=$dbp["countrow"];
			$res_cos_id=$dbp["cos_id_study"];
			$status_reserve_done="yes";
			$rd1=explode("-",$dbp["dateStudy"]);
			$res_date=$rd1[2]."-".$rd1[1]."-".$rd1[0];
			if($_GET["mod"]!="res"){
			 echo "<script>alert('คูณทำการจองที่นั่งสำหรับการเรียนชดเชยนี้แล้ว วันที่ $res_date ถ้าคุณทำการจองที่นั่งใหม่ ระบบจะยกเลิกการจองสำหรับที่นั่งเก่าอัติโนมัติ')</script>";
				}
			}else{
				$status_reserve_done="no";
				}
	 
	 ///start cal date //////////////
//$DateStart=date("Y-m-d");//- 1. แปลงวันที่ให้เป็น timestamp ก่อน เพื่อเอามาคำนวณ วันเวลาล่วงหน้าได้

$strtime = strtotime($cosnow_enddate);

//- 2. คำนวณ วันเวลาล่วงหน้าได้ ด้วยการ บวกเดือนเพิ่มอีก 2 เดือน

//$caltime=strtotime("+1 Month",$strtime);
$caltime1=strtotime("+3 Month",$strtime);

//- 3. จากค่าด้านบนยังคงเป็นtimestamp อยู่ดังนั้นนต้องแปลงย้อนกลับไปเป็นวันที่ปกติี่

$threemonth=date("Y-m-d", $caltime1);
	 
	 // $notisql="select * from pf_course where sub_id='$sub_id' and cos_subject_type='MC' and std_course_typeID='$std_course_typeID' and cos_date_end>='$today' and cos_date_end<='$threemonth'  order by cos_id";
	 
	 // $notisql="select * from std_course_create_detail where sub_id='$sub_id' and cos_subject_type='MC' and dateStudy>='$today' and dateStudy<='$threemonth' $sqlstrtime and topic='$topic' order by cos_id";
	   // $notisql="select * from std_course_create_detail where sub_id='$sub_id' and cos_subject_type='MC' and dateStudy>='$today' and dateStudy<='$threemonth' $sqlstrtime and ($str_alldayNo) order by cos_id";
		 $notisql="select * from std_course_create_detail where sub_id='$sub_id' and cos_subject_type='MC' and dateStudy>='$today' and dateStudy<='$threemonth' $sqlstrtime and dayNo='$dayNo' order by cos_id";
	  //echo "$notisql<br>";
	  $rsnoti=mysql_query($notisql) or die ("ดูข้อมูลรายการวันเรียนชดเชยไม่ได้");
	
	if(mysql_num_rows($rsnoti)==0){
		echo "<script>alert('ไม่พบเวลาทีทำการเรียนชดเชยได้ กรุณาติดต่อกับทางเจ้าหน้าที่ 02-252-8633')</script>";
		exit();
		}
	  
	 
}// end for ตรวจว่ามีเวลาเรียนมั้ย

?>
        <table class="table table-bordered" style="width:100%">        
	      <tr bgcolor="#FFCCFF">
					      <th bgcolor="#FCC">ลำดับ</th>
					      <th bgcolor="#FCC">วันที่เรียน</th>
					      <th bgcolor="#FCC">เวลา</th>
					      <th bgcolor="#FCC">จองที่นั่งเรียน</th>
          </tr>
                          <? 
						
						for($j=0;$j<$count_topic;$j++){	

$topic=$topic_a[$j];
$dayNo=$dayNo_a[$j];
//echo "$i/topic==$topic//$dayNo<br>";

 //$notisql="select * from std_course_create_detail where sub_id='$sub_id' and cos_subject_type='MC' and dateStudy>='$today' and dateStudy<='$threemonth' $sqlstrtime and topic='$topic' order by dateStudy,time_start asc";
 $notisql="select * from std_course_create_detail where sub_id='$sub_id' and cos_subject_type='MC' and dateStudy>='$today' and dateStudy<='$threemonth' $sqlstrtime and dayNo='$dayNo' order by dateStudy,time_start asc";
	 // echo "$notisql<br>";
	  $rsnoti=mysql_query($notisql) or die ("ดูข้อมูลรายการวันเรียนชดเชยไม่ได้");
						
						while($dbnot=mysql_fetch_array($rsnoti)){
							
							$no=$no+1;
							$typeID=$dbnot["typeID"];
							$dayNo=$dbnot["dayNo"];
							$cos_id_s=$dbnot["cos_id"]; //echo "cos_id_s==$cos_id_s";
							$dateStudy=$dbnot["dateStudy"];
							$dayName=date('D', strtotime($dateStudy));
							$sub_id=$dbnot["sub_id"];
							$ssub="select * from pf_subject where sub_id='$sub_id'";
							//echo "$ssub<br>";
							$rssub=mysql_query($ssub) or die ("ดูข้อมูลวิชาไม่ได้");
							$dbsub=mysql_fetch_array($rssub);
							$sub_name=$dbsub["sub_name"];
							
							$time_start=$dbnot["time_start"];
							$time_end=$dbnot["time_end"];
							$dateCreate=$dbnot["dateCreate"];
							$createBy=$dbnot["createBy"];
						
						
					// ตรวจสอบว่าคุณจองที่นั่งเรียนวันนี้หรือเปล่า ///////	
		$sqlp11="select * from std_course_reserve where  dayNo='$dayNo' and dateStudy='$dateStudy' and std_id='$std_id'";
		//echo "$sqlp1<br>";
		$rsp11=mysql_query($sqlp11) or die ("ดูข้อมูลการจองของคุณไม่ได้");
		if(mysql_num_rows($rsp11)>0){
			$status_reserve_done="yes";
			
			}else{
				$status_reserve_done="no";
				}		
							
							?>
					       <tr <? if($status_reserve_done=="yes" and ($res_cos_id==$cos_id_s)){?> bgcolor="#FF6699"<? }?>>
					      <td><span id='snum'><? echo $no;?>.</span></td>
					      <td><? 
	
	$datestr1=explode("-",$dateStudy);
	$d1=$datestr1[2];
	$m1=$datestr1[1];
	$y1=$datestr1[0];
	$dateStr="$d1-$m1-$y1";
	echo "$topic<br>";
						  echo "[$dayName] ".$dateStr;
						  ?></td>
					      <td><? echo "$time_start-$time_end";?></td>
					      <td><? 
				
					
			// ตรวจสอบว่ามีคนจองที่นั่งเรียนชดเชย cos นี้กี่คนแล้ว ///////	
		//$sqlp1="select * from std_course_reserve where cos_id_from='$cos_id_chk' and cos_id_study='$cos_id_s' and topic='$topic' and std_id='$std_id'";
		$sqlp1="select * from std_course_reserve where  dayNo='$dayNo' and dateStudy='$dateStudy'";
		//echo "$sqlp1";
		$rsp1=mysql_query($sqlp1) or die ("ดูข้อมูลการจองไม่ได้");
		$numnow=mysql_num_rows($rsp1);
		//echo "$numnow<br>";
			
					$numcanreserve=(3-$numnow) ; 
					//echo "numcan==$numcanreserve";
					if($numcanreserve<=0){
						$status_reserve="no";
						echo "-";
						}else{
								echo "<div style=\"float:left; padding:1px;margin-right:5px; position:relative;\">ที่นั่งว่าง: <strong style=\"color:#339900;\"> $numcanreserve</strong></div> ";
						?>
						<div style="float:left; padding:1px; position:relative;">
						<?	
							for($i=1;$i<=$numcanreserve;$i++){
								echo "<img src=\"https://chulatutor-chulatutor.netdna-ssl.com/images/s-chair.png\">";
								} // end for
								?>
							</div>
								<?
							$status_reserve="yes";
						}
						 // echo "status==$status_reserve";
						  ?>
					        <?
                          
						  //echo "cos_idchk=$cos_id_chk/$cos_id_s<br>";
						//  echo "status_reserve_done==$status_reserve_done";
						  if($status_reserve_done=="yes"){
					?>
					
					<!--<div id="cancel-btn"><a href="../reserve_seat2/">ยกเลิก</a></div> -->
					<?		  
							 echo "<div id=\"cancel-btn\"><a href=\"../reserve_seat2/?mod=cancel&cos_id_s=$cos_id_s&cos_id_f=$cos_id_chk&std_id=$std_id&topic=$topic&dayNo=$dayNo&timeall=$time_start-$time_end&date_off=$date_off&datestudy=$dateStudy&sub_id=$sub_id&typeID=$typeID&dateSearch=$date_off\">ยกเลิก</a></div>";
							  }elseif($numcanreserve>0){
							  
							 echo "<div id=\"reserv-btn\"><a href=\"../reserve_seat2/?mod=res&cos_id_s=$cos_id_s&cos_id_f=$cos_id_chk&std_id=$std_id&topic=$topic&dayNo=$dayNo&timeall=$time_start-$time_end&date_off=$date_off&datestudy=$dateStudy&sub_id=$sub_id&typeID=$typeID&dateSearch=$date_off\">จอง</a></div>";  
							  }else{
								  echo "ขออภัยค่ะ ที่นั่งชดเชยเต็มแล้ว";
								  }
						  
						  ?></td>
				        </tr>
                        
                        <? } // end while?>
	         <? 
			 
			// echo "last===$j<br>";
			 
			 } // end for ขาดเรียนกี่หัวข้อ?>
	        </table>
                      
                     
                    
					 
        
	  </div>
	<div style="font-size:14px;"> หากท่านไม่พบเวลาสะดวกหรือติดปัญหาการจองที่นั่งเรียนชดเชยกรุณาติดต่อกับทางเจ้าหน้าที่ <font size="+1" style="color:#3366FF;">02-252-8633</font> </div>
    	<? } //end if std_phone !==''?>
	</div><!-- /container -->	
          
  
        </article>
  
  
  </div>
  
</div>


<?php
get_footer();
?>