<?php 
/*
* Template Name: Portfolio
*/

get_header();
?>
<div class="portfolio header">
  <div class="row">
    <h2><?php _e('PORTFOLIO', 'chulatutor'); ?></h2>
  </div>
</div>
<div class="portfolio stat">
  <div class="row">
    <div class="medium-6 large-8 column"><img src="<?php the_field('stat_image', 'option'); ?>"></div>
    <div class="medium-6 large-4 column">
      <p class="title"><?php the_field('stat_title_1', 'option'); ?></p>
      <p class="number"><?php the_field('stat_number_1', 'option'); ?></p>
      <p class="title"><?php the_field('stat_title_2', 'option'); ?></p>
      <p class="number"><?php the_field('stat_number_2', 'option'); ?></p>
      <p class="title"><?php the_field('stat_title_3', 'option'); ?></p>
    </div>
  </div>
</div>
<div class="portfolio desc">
  <div class="row expanded">
    <div class="text large-6 column">
      <div class="medium-9 medium-offset-2">
        <p><strong><?php the_field('previous_title', 'option'); ?></strong></p>
        <?php the_field('previous_description', 'option'); ?>
      </div>
    </div>
    <div class="img large-6 column"></div>
  </div>
</div>
<div class="portfolio video">
  <div class="row">
    <h3>วิดีโอผลงานของเรา</h3>
  </div>
  <div class="row">
    <div class="slide-group medium-10 medium-centered">
      
      <?php        
        $portfolio_video = get_field('portfolio_video', 'option');  
        if ( $portfolio_video ) :
          foreach ($portfolio_video as $key => $val) :
      ?> 
      <div class="item small-12 medium-6 column">
        <div class="hover"></div>
        <a class="youtube-popup" href="<?php echo $val["video"]; ?>">
          <img src="<?php echo $val["image"]; ?>">
        </a>
      </div>
      <?php
          endforeach;
        endif;
      ?>
      
    </div>
  </div>
</div>
<div class="portfolio co-op">
  <div class="row">
    <h3>ร่วมงานกับองค์กรชั้นนำ<br>ในการพัฒนาการศึกษาไทย</h3>
    
    <?php        
      $coperate_with = get_field('coperate_with', 'option');  
      $c = 1;
      if ( $coperate_with ) :
        foreach ($coperate_with as $key => $val) :
          if ( $c == 1 ) 
            $class = "medium-offset-1";
          else
            $class = "";
    ?> 
    <div class="column small-6 medium-2 <?php echo $class; ?>">
      <div class="item"><img src="<?php echo $val["url"]; ?>"></div>
    </div>
    <?php
          $c++;
        endforeach;
      endif;
    ?>

    <div class="column medium-1 show-for-medium"></div>
  </div>
</div>
<div class="portfolio speaker show-for-medium">
  <div class="row">
    <h3>เป็นวิทยากรให้กับ</h3>
    <div class="medium-8 medium-centered column">
      <?php        
        $instructor_for = get_field('instructor_for', 'option');  
        if ( $instructor_for ) :
          foreach ($instructor_for as $key => $val) :
      ?>
      <div class="medium-3 column"><img src="<?php echo $val["url"]; ?>"></div>
      <?php
          endforeach;
        endif;
      ?>
    </div>
  </div>
</div>
<?php
get_footer();
?>