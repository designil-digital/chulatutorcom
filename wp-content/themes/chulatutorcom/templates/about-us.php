<?php 
/*
* Template Name: About Us
*/

get_header();
?>
<div class="about header" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/about/header-bg.jpg');">
  <div class="row">
    <div class="table">
      <div class="table-cell">
        <h2>ABOUT US</h2>
      </div>
    </div>
  </div>
</div>
<div class="about detail">
  <div class="row">
    <div class="img large-3 column"><img src="<?php the_field('abous_us_logo', 'option'); ?>">
      <div class="line-container show-for-large">
        <div class="line"></div>
      </div>
    </div>
    <div class="large-9 column">
      <div class="medium-8 medium-centered large-12">
        <p class="title"><?php the_field('about_us_since', 'option'); ?></p>
        <?php the_field('about_us_history', 'option'); ?>
      </div>
    </div>
  </div>
</div>
<div class="about timeline">
  <div class="row">

    <?php        
      $history = get_field('history', 'option');  
      if ( $history ) :
        foreach ($history as $key => $val) :
    ?> 
    <div class="item medium-8 medium-centered large-10 large-offset-1">
      <div class="dot show-for-large"></div>
      <p class="title"><?php echo $val["year"]; ?></p>
      <?php echo $val["history"]; ?>
      <div class="row large-10 logo-list">
        <?php
        if ( $val["logo"] ) :
          foreach ($val["logo"] as $key2 => $val2) :
        ?>
        <div class="img small-6 medium-4 large-3 column end">
          <img src="<?php echo $val2["logo"]; ?>">
          <?php echo $val2["text"]; ?>
        </div>
        <?php
          endforeach;
        endif;
        ?>
      </div>
    </div>
    <?php
        endforeach;
      endif;
    ?>

  </div>
  <div class="clearfix"></div>
</div>
<div class="about mission">
  <div class="row">
    <div class="medium-8 medium-centered column">
      <h3><?php the_field('goal', 'option') ?></h3>
      <?php the_field('goal_description', 'option'); ?>
    </div>
  </div>
</div>
<?php
get_footer();
?>