<?php 
/*
* Template Name: Contact Us
*/

get_header();
?>
<div class="contact header">
  <h2>CONTACT US</h2>
</div>
<div class="contact map">
  <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/contact/map.png"></div>
</div>
<div class="contact detail">
  <div class="row medium-8 medium-centered">
    <div class="large-4 column">
      <p><strong>เวลาเปิดทำการ</strong></p>
      <?php the_field('time_open', 'option'); ?>
      <hr>
      <p><strong>ติดต่อสมัครเรียน</strong></p>
      <?php the_field('register_contact', 'option'); ?>
    </div>
    <div class="large-4 column">
      <p><strong>ติดต่อ</strong></p>      
      <?php the_field('contact_info', 'option'); ?>
      <hr>      
      <?php the_field('contact_info_2', 'option'); ?>
    </div>
    <div class="large-4 column form">
      <label>ส่งข้อความ</label>
      <?php echo do_shortcode(get_field('contact_form', 'option')); ?>
    </div>
  </div>
</div>
<?php
get_footer();
?>