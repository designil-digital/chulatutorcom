<?php 
/*
* Template Name: Tutor Register
*/

get_header();
?>
<div class="tutor-register header">
  <h2>TUTOR REGISTER</h2>
</div>
<div class="tutor-register intro">
  <div class="img medium-4 column">
    <div class="img1" style="background-image:url('<?php the_field('tutor_image_1', 'option'); ?>');"></div>
    <div class="img2 show-for-medium" style="background-image:url('<?php the_field('tutor_image_2', 'option'); ?>');"></div>
  </div>
  <div class="detail medium-8 large-5 column end">
    <?php the_field('tutor_register', 'option'); ?>
  </div>
  <div class="clearfix"></div>
</div>
<div class="tutor-register form">
  <div class="medium-10 medium-centered large-6 large-centered column row">
    <h3>สมัครติวเตอร์</h3>
    <?php echo do_shortcode(get_field('register_form', 'option')); ?>    
  </div>
</div>
<?php
get_footer();
?>