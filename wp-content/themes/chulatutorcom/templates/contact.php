<?php session_start();
/*
* Template Name: contact
*/

get_header();
?>
  
   <script language="javascript1.4" type="text/javascript">
<!--
 

	function chknull(){
		var str='Please enter ';
	  if(document.form1.name.value==''){
			alert(str+'Name');
			document.form1.name.focus();
			return false;
	  }else if(document.form1.subject.value==''){
			alert(str+'Subject');
			document.form1.subject.focus();
			return false;
			
		}else if(document.form1.tel.value==''){
			alert(str+'Tel');
			document.form1.tel.focus();
			return false;
		
		}else if(isNaN(document.form1.tel.value)){
			alert ('กรอกเบอร์โทรได้แต่ตัวเลขเท่านั้น'); 
			document.form1.tel.focus();
 
			return false;
		
		}else if(document.form1.email.value==''){
			alert(str+'Email');
			document.form1.email.focus();
			return false;
			
		}else if(!validateForm()){
			//alert(str+'Email');
			document.form1.email.focus();
			return false;	

		}else if(document.form1.code.value==''){
			alert(str+'Anti Spam');
			document.form1.code.focus();
			return false;
		 
		}else{
			return validateForm();
		}
	}
 
	function validateForm()
	{
	var x=document.forms["form1"]["email"].value;
	var atpos=x.indexOf("@");
	var dotpos=x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
		  {
		  alert("รูปแบบ e-mail ไม่ถูกต้อง");
			 return false;
			 document.form1.tea_email.focus();
		  }else{
			  return ButtonClicked();
		  }
	}
-->
</script>
<div class="tutor-register header">
  <h2>Contact Us</h2>
</div>
<div class="tutor-register intro">
  <div class="img medium-4 column">
    <div class="img1" style="background-image:url('<?php the_field('tutor_image_1', 'option'); ?>');"></div>
    <div class="img2 show-for-medium" style="background-image:url('<?php the_field('tutor_image_2', 'option'); ?>');"></div>
  </div>
  <div class="detail medium-8 large-6 column end">
   
    
       ติดต่อเรา สถาบันกวดวิชาจุฬาติวเตอร์
     
         
           
                                                                  <table width="100%" border="0" cellspacing="0" cellpadding="7">
                                                                    <tr>
                                                                      <td width="45%" valign="top"><strong class="cu-best">เปิดทำการ:</strong></td>
                                                                      <td width="23%"><span class="cu-aat">วันจันทร์ - ศุกร์     <br />
วันเสาร์ - อาทิตย์  </span><br /></td>
                                                                      <td width="32%" valign="top" class="sat">เวลา 09.00 - 20.30 น.<br />
                                                                      เวลา 08.30 - 17.00 น. </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top">&nbsp;</td>
                                                                      <td colspan="2" class="cutep">(ไม่เว้นวันหยุดนักขัตฤกษ์)</td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top"><strong class="get">ที่อยู่:</strong></td>
                                                                      <td colspan="2" class="gmat">430/27-28 สยามสแควร์ ซอย9<br />
                                                                        ถ.พระราม 1 แขวงปทุมวัน เขตปทุมวัน กรุงเทพฯ<br />
                                                                      (ชั้น 2-4 ตรงข้ามศูนย์หนังสือจุฬาฯ สยาม ) </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top" class="sat"><strong>ติดต่อสมัครเรียน</strong>:</td>
                                                                      <td colspan="2" class="cutep">02-252-8633</td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top" class="cu-tep"><strong>E-mail:</strong></td>
                                                                      <td colspan="2" class="cutep"><a href="mailto:admin@chulatutor.com">admin@chulatutor.com</a></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top" class="cu-tep"><strong>ติดต่อจัดอบรม, ฝ่ายจัดซื้อ, ฝ่ายการตลาด:</strong></td>
                                                                      <td colspan="2" class="cutep"><a href="mailto:chulatutor@live.com">chulatutor@live.com</a></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top" class="igcse"><strong>ต้องการติชมการเรียนการสอน หรือการให้บริการไม่เป็นที่พอใจ:</strong></td>
                                                                      <td colspan="2" valign="top" class="cutep"><strong><a href="mailto:chulatutor@windowslive.com">chulatutor@windowslive.com</a></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top" class="igcse"><strong>ติดต่อปัญหาการใช้งานบนเว็บไซต์</strong>:</td>
                                                                      <td colspan="2" valign="top" class="cutep"><strong><a href="mailto:webmaster@chulatutor.com">webmaster@chulatutor.com</a></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td valign="top" class="igcse"><strong>แผนที่:</strong></td>
                                                                      <td colspan="2" class="cutep"><a href="map_chulatutor.php" target="_blank"><img src="https://chulatutor-chulatutor.netdna-ssl.com/images/map-chulatutor-small.jpg" alt="map_chulatutor" width="300" border="0" /><br />
คลิกดูเพื่อดูภาพใหญ่</a></td>
                                                                    </tr>
                                                                  </table>
                                                                  <br />
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                      <td align="center" class="bule">หากท่านต้องการจะสอบถามข้อมูลเพิ่มเติมกรุณากรอกข้อมูลด้านล่าง <br />
                                                                      Chulatutor จะรีบติดต่อกลับหาท่านภายใน 24 ชั่วโมง</td>
                                                                    </tr>
            </table>
                                                                  <br />
                                                                  <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                      <td align="left"><div align="left"><font color="#0074BD"><strong><font color="#FF0000">*</font></strong></font><strong class="gmat">ข้อมูลที่จำเป็นต้องกรอก (กรุณากรอกข้อมูลตามจริง)</strong></div></td>
                                                                    </tr>
                                                                  </table>
                                                                  <br />
                                                                  <?php

		?>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                      <td><form name="form1"  enctype='multipart/form-data'  onsubmit='return chknull()' method='post'  action="" >
                                                                            <table width="100%" border="0" cellpadding="8" cellspacing="0" class="table table-bordered">
                                                                                  <tr>
                                                                                    <td width="36%" align="right"><div align="right"><span class="cu-tep"><strong><font color="#FF0000">*</font> ชื่อ:</strong></span></div></td>
                                                                                    <td width="64%" align="left"><div align="left"><span class="cu-tep"><font color="#0066CC">
                                                                                      <input name="name" type="text" id="name" value="<?php if($_POST['name']) { echo $_POST['name']; } ?>" size="35" class="form-control" style="width:80%; float:left; margin-right:2px;" />
                                                                                    </font></span></div></td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                    <td align="right"><div align="right"><span class="cu-tep"><strong><font color="#FF0000">*</font> อีเมล์:</strong></span></div></td>
                                                                                    <td align="left"><div align="left"><span class="cu-tep"><font color="#0066CC">
                                                                                      <input name="email" type="text" id="email" value="<?php if($_POST['email']) { echo $_POST['email']; } ?>" size="35" style="width:80%; float:left; margin-right:2px;"/>
                                                                                    </font></span></div></td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                    <td><div align="right"><span class="cu-tep"><strong><font color="#FF0000">*</font> โทรศัพท์:</strong></span></div></td>
                                                                                    <td align="left"><div align="left"><span class="cu-tep"><font color="#0066CC">
                                                                                      <input name="tel" type="text" id="tel" value="<?php if($_POST['tel']) { echo $_POST['tel']; } ?>" size="35" style="width:50%; float:left; margin-right:2px;"/>
                                                                                    </font></span></div></td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                    <td align="right"><div align="right"><span class="cu-tep"><strong><font color="#FF0000">*</font> เรื่องที่ต้องการติดต่อ:</strong></span></div></td>
                                                                                    <td align="left"><div align="left"><span class="cu-tep"><font color="#0066CC">
                                                                                      <input name="subject" type="text" id="subject" value="<?php if($_POST['subject']) { echo $_POST['subject']; } ?>" size="45" style="width:80%; float:left; margin-right:2px;" />
                                                                                    </font></span></div>                                                                                      </td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                    <td><div align="right"><span class="cu-tep"><strong><font color="#FF0000">*</font> ข้อความ: </strong></span></div></td>
                                                                                    <td align="left"><div align="left"><span class="cu-tep"><font color="#0066CC">
<textarea name="message" cols="40" rows="10" id="text" style="width:80%; float:left; margin-right:2px;"><?php if($_POST['message']) { echo $_POST['message']; } ?></textarea>
                                                                                    </font></span></div></td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                    <td align="right">
                            <strong><font color="#FF0000">*</font></strong> <strong>กรุณากรอกตัวเลขตามที่ปรากฏ</strong>                                                           <div style="margin-right:2px;">
<img src="../captcha/CaptchaSecurityImages.php?width=100&amp;height=40&amp;characters=4" /></div>
</td>
                                                                                    <td align="left">
                                                                                      <input type="text" name="code" style="width:30%; float:left; margin-right:2px;" />
                                                                                     <input type="submit" class="submit" name="submit" value="Send message" style="width:110px;" /></td>
                                                                                  </tr>
                                                                                </table>
                                                                          </form></td>
                                                                        </tr>
                                                                      </table>
  </div>
</div>

	<?php
		if (isset($_POST['submit'])) {
		$error = "";

		if (!empty($_POST['subject'])) {
		$subject = $_POST['subject'];
		} else {
		$error .= "You didn't type in your subject. <br />";
		}
		
		if (!empty($_POST['name'])) {
		$name = $_POST['name'];
		} else {
		$error .= "You didn't type in your name. <br />";
		}
		
		if (!empty($_POST['tel'])) {
		$tel = $_POST['tel'];
		} else {
		$error .= "You didn't type in your tel. <br />";
		}

		if (!empty($_POST['email'])) {
		$email = $_POST['email'];

		} else {
		$error .= "You didn't type in an e-mail address. <br />";
		}

		if (!empty($_POST['message'])) {
		$message = $_POST['message'];
		} else {
		$error .= "You didn't type in a message. <br />";
		}

		if(($_POST['code']) == $_SESSION['code']) { 
		$code = $_POST['code'];
		} else { 
		
		echo "<script>alert('คุณกรอกข้อมูลหมายเลขยืนยันไม่ถูกต้อง')</script>";
		$error .= "The captcha code you entered does not match. Please try again. <br />";    
		
		}

		if (empty($error)) {
		$from = 'From: ' . $name . ' <' . $email . '>';
		$from .= " \r\n";
		$from .= "Content-type: text/html; charset=utf-8\r\n";		
		$to = "chulatutor@live.com";
		$subject = "$subject";
		$content = "
	<table width=500 border=0 cellpadding=0 cellspacing=0>
	 <tr>
	 <td width=30% align=right valign=top><strong>Name : </strong></td>
	 <td width=2% align=left valign=top>&nbsp;</td>
	 <td width=68% align=left valign=top><font color=#0099CC>$name</font></td>
	 </tr>
	 <tr>
	 <td width=30% align=right valign=top><strong>Email : </strong></td>
	 <td width=2% align=left valign=top>&nbsp;</td>
	 <td width=68% align=left valign=top><font color=#0099CC>$email</font></td>
      </tr>
	 <tr>
	 <td width=30% align=right valign=top><strong>Tel : </strong></td>
	 <td width=2% align=left valign=top>&nbsp;</td>
	 <td width=68% align=left valign=top><font color=#0099CC>$tel</font></td>
      </tr>
	 <tr>
	 <td width=30% align=right valign=top><strong>Subject : </strong></td>
	 <td width=2% align=left valign=top>&nbsp;</td>
	 <td width=68% align=left valign=top><font color=#0099CC>$subject</font></td>
      </tr>
	 <tr>
	 <td width=30% align=right valign=top><strong>Message : </strong></td>
	 <td width=2% align=left valign=top>&nbsp;</td>
	 <td width=68% align=left valign=top><font color=#0099CC>$message</font></td>
      </tr>
	</table>
    <br>
";


include("phpmailer/smtp-gmail.php");	

		if($name!=''){
		smtpmailer($to, $from, $name, $subject, $content);
		echo "<script>alert('Chulatutor ได้รับอีเมล์จากท่านแล้ว จะรีบติดต่อกลับภายใน 24 ชั่วโมง ขอบคุณค่ะ');window.location='index.php'</script>";
		}

		
		
		}
	}
		?>
<?php
get_footer();
?>