<?php session_start();
/*Template Name: exam
*/
get_header();
?>
 
<div class="tutor-register header">
  <h2>สมัครสอบ TOEIC</h2>
</div>
<div class="tutor-register intro">
  <div class="img medium-4 column">
    <div class="img1" style="background-image:url('<?php the_field('tutor_image_1', 'option'); ?>');"></div>
    <div class="img2 show-for-medium" style="background-image:url('<?php the_field('tutor_image_2', 'option'); ?>');"></div>
  </div>
  <div class="detail medium-8 large-7 column end">
 
        <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="100%" valign="top"><table width="100%" height="35" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td background="images/icon_header.png"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="35"><h1 class="white" style="margin-left:20px;">เช็คผลการสมัครสอบ TOEIC</h1></td>
            </tr></table></td>
        </tr>
      <tr>
        <td><form name='form2' method='post'  action="toeic_check.php"><table width='100%'  border="0" cellpadding="1" cellspacing="1" >
          
          
          <tr>
            <td width="87%" height="34" align='center'><h2>กรอกเบอร์โทรที่ใช้สมัคร</h2>
              <input type='text'   name='txtFind_tec_tel'  style="width:40%">
              <input type='submit' name='search'  value='เช็คผลการสมัคร' style="width:110px;">
              <input name="page" type="hidden" id="page" value="toeic_check.php"></td></tr>
  </table>
  </form></td>
        </tr>
      <tr>
        <td background="images/icon_header.png"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"> <tr>
          <td height="35"><h1 class="white" style="margin-left:20px;">สมัครสอบ TOEIC</h1></td>
          </tr>
          
          
          </table></td>
        </tr>
      <tr>
        <td></td>
        </tr>
      </table>
      <br />
      <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td width="97%" class="sat"> 
            <form action="exam_c.php" method="post" enctype="multipart/form-data" name="form1" id="form1"  onsubmit='return chknull()'>
              <table width="100%" border="0" align="center" cellpadding="2" cellspacing="4">
                
                <tr>
                  <td align="center"><table width="100%" border="0" align="center" cellpadding="4" cellspacing="1"  class="table table-bordered">
                    
                    <tr>
                      
                      <td title="<?=$tec_id?>">วันที่แจ้ง</td>
                      <td>&nbsp;
                        <?=date('Y-m-d H:i');?>
                        <input name="page" type="hidden" id="page" value="exam_f.php" /></td>
                      </tr>
                    <tr>
                      <td width="35%">ชื่อ - นามสกุล (ภาษาอังกฤษ)</td>
                      <td width="65%"><input name="tec_name" type="text" id="tec_name" size="35" /></td>
                      </tr>
                    <tr>
                      <td>ต้องการ</td>
                      <td><label>
                        <input name="tec_past"   type="radio" value="สมัครสอบ" checked="checked" />
                        สมัครสอบ</label><label>
                          <input name="tec_past"   type="radio" value="เลื่อนสอบ" />
                          เลื่อนสอบ</label>
                        <label>
                          <input name="tec_past" type="radio"   value="ยกเลิก" />
                          ยกเลิก</label>
                        <label></label></td>
                      </tr>
                    <tr>
                      <td>รหัสบัตรประชาชน (13 หลัก) </td>
                      <td>
                        <input name="tec_card" type="text" id="tec_card" size="35" maxlength="13" />
                        <br />
                        <div align="left" style="color:#999999; font-size:11px">กรอกไม่ต้องใส่ &quot;-&quot; เช่น 1234567543213 </div></td>
                      </tr>
                    <tr>
                      <td>เบอร์โทร (10 หลัก) </td>
                      <td><input name="tec_tel" type="text" id="tec_tel" size="22" maxlength="10" />
                        <br />
                        <div align="left" style="color:#999999; font-size:11px">กรอกไม่ต้องใส่ &quot;-&quot; เช่น 0801112222 </div></td>
                      </tr>
                    <tr>
                      <td>อีเมล์</td>
                      <td><input name="tec_email" type="text" id="tec_email" size="35" /></td>
                      </tr>
                    <tr>
                      <td>วันที่สอบ 1 </td>
                      <td><input name="tec_test_date" type="text" id="tec_test_date" size="22"  />
                        <select name="tec_test_date_time" style="width:40%">
                          <option value="" selected="selected">=เวลาสอบ=</option>
                          <option value="9.00-11.30">9.00-11.30</option>
                          <option value="13.00-15.00">13.00-15.00</option>
                          </select>
                        </td>
                      </tr>
                    <tr>
                      <td valign="top">วันที่สอบ 2 <br />
                        <span style="font-size:11px; color:#999999">(สำหรับในกรณีวันที่เลือกวันแรกไม่ได้)</span></td>
                      <td valign="top">
                      
                       
                      <input name="tec_test_date2" type="text" id="tec_test_date2" size="22" />
                        <select name="tec_test_date_time2" id="tec_test_date_time2" style="width:40%">
                          <option value="" selected="selected">=เวลาสอบ=</option>
                          <option value="9.00-11.30">9.00-11.30</option>
                          <option value="13.00-15.00">13.00-15.00</option>
                          </select>
                        </td>
                      </tr>
                    <tr>
                      <td valign="top"> วันที่สอบ 3<br />
                        <span style="font-size:11px; color:#999999"> (สำหรับในกรณีวันที่เลือกวันที่สอบไม่ได้)         </span>               </td>
                      <td valign="top"><input name="tec_test_date3" type="text" id="tec_test_date3" size="22" />
                        <select name="tec_test_date_time3" id="tec_test_date_time3" style="width:40%">
                          <option value="" selected="selected">=เวลาสอบ=</option>
                          <option value="9.00-11.30">9.00-11.30</option>
                          <option value="13.00-15.00">13.00-15.00</option>
                          </select>
                        <br />
                        <label></label></td>
                      </tr>
                    <tr>
                      <td height="34">รูปภาพ</td>
                      <td><input name="tec_pic" type="file" id="tec_pic" size="35" /></td>
                      </tr>
                    <tr >
                      <td align='left' valign="top"   >Number anti spam. <strong><br />
                        </strong></td>
                      <td align="left" valign="top"    ><label for="security_code"><strong>
                        <input name="security_code" type="text" id="security_code" size="6" style="width:20%"/>
                        </strong></label>
                        <br />
                        <strong><img src="captcha/CaptchaSecurityImages.php?width=100&amp;height=40&amp;characters=4" /></strong></td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td><input type="submit" name="Submit" value="Submit" />
                        <input type="reset" name="Submit2" value="Reset" /></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </form>
            <h1 style="color:#333333;font-size:20px">สมัครสอบ TOEIC ผ่านทางสถาบันจุฬาติวเตอร์ เพื่อรับส่วนลด</h1>
            <p><strong style="color:#FF3399">ขั้นตอนการสมัครสอบ TOEIC</strong></p>
            <p>สมัครผ่านทางสถาบัน จะได้รับส่วนลดค่าสอบ จาก 1,500฿ เป็น 1,200฿ ตลอดปี<br />
              ผู้สมัครสอบต้องมีบัตรนักเรียนก่อนสมัครสอบ  (หากไม่มีบัตรกรุณาติดต่อเจ้าหน้าที่ )<br />
              สมัครโดยตรงผ่านทาง เว็บไซค์ของจุฬาติวเตอร์ ทางลิงค์ www.chulatutor.com/exam เท่านั้น<br />
              สมัครก่อนวันสอบอย่างน้อย 3 -5 วัน  เพื่อหลีกเลี่ยงที่นั่งสอบเต็ม</p>
            <p> กรณียกเลิก หรือเลื่อนวันสอบ ต้องยกเลิกก่อนอย่างน้อย  2  วัน ภายในวัน จ-ศ  ตามที่ลิงค์</p>
            <p>www.chulatutor.com/exam   ของสถาบันเท่านั้น หากสมัครแล้วไม่ได้ไปสอบวันที่จองไว้</p>
            <p>และไม่มีการยกเลิกการสอบกับทางสถาบัน ในการสอบครั้งต่อไปทางศูนย์สอบจะปรับ 500฿</p>
            <p>·         เมื่อสมัครสอบแล้ว ผู้สมัครต้องการตรวจสอบสถานะ การสมัครสอบสามารถเข้าไปตรวจสอบในลิงค์ของwww.chulatutor.com/exam  เช่นเดิม </p>
            <p><strong style="color:#FF3399">ขั้นตอนในการตรวจเช็คสถานะสมัครสอบ</strong></p>
            <p>·         กรอกเบอร์โทรศัพท์ที่ช่อง  “เช็คผลการสมัครสอบ TOEIC ”</p>
            <p>·         กรอกข้อมูลสมัครสอบในช่วงวัน จ- ศ เวลา 9.00 – 15.00 น. รอการดำเนินการ ภายใน 24 ชม.              กรอกข้อมูลสมัครสอบในช่วงวัน ส – อา เวลา 9.00 – 15.00  รอการดำเนินการ 2 วันเนื่องจากปิดทำการ</p>
            <p>·         เมื่อขึ้นสถานะว่า “ดำเนินการแล้ว”  ให้โทรไปเช็คที่ศูนย์สอบโทอิค เบอร์ 02-260-7061 ต่อ 101  ในวันถัดไป โดยแจ้งเลขที่บัตรประชาชนกับทางเจ้าหน้าที่ ถ้ามีชื่อในวันสอบ ถือว่าการสมัครเสร็จสิ้น</p>
            <p>วันสอบควรไปก่อนเวลาสอบ 1 ชม. เพื่อลงทะเบียนถ่ายรูปและชำระเงินค่าสอบ<br />
              สิ่งที่เตรียมไปวันสอบ คือ บัตรประชาชน และ บัตรนักเรียนของสถาบัน พร้อมค่าสอบ 1,200฿<br />
              </p>
            <p><strong style="color:#FF3399">สถานที่สอบ</strong>   TOEIC Services Suite ศูนย์สอบกรุงเทพ <br />
              อาคาร BB Building, 1907 ชั้น 19   54  ถนนอโศก  สุขุมวิท <br />
              โทร. 02-260-7061, 02-259-3990 </p>
            <p> (ศูนย์สอบปิดทำการ ทุกวันอาทิตย์และวันหยุดนักขัตฤกษ์)</p>
            <p></p>          <h3 class="head_pink">&nbsp;</h3>
          </td>
          </tr>
        </table>   
</td>
    </tr>
</table>

</div>
                
</div>
<?php
get_footer();
?>