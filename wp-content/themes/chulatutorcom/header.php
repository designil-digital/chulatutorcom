<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chulatutorcom
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>
	<!-- <div class="page-nave">
		<div class="row">
			<div class="column">
				<ul>
					<li> <a href="./index.html">Home</a></li>
					<li><a href="./course.html">Course</a></li>
					<li><a href="./portfolio.html">Portfolio</a></li>
					<li><a href="./tutor.html">Tutor Detail</a></li>
					<li><a href="./tutor-register.html">Tutor Register</a></li>
					<li><a href="./about.html">About Us</a></li>
					<li><a href="./contact.html">Contact Us</a></li>
					<li><a href="./single.html">Single Page Template</a></li>
				</ul>
			</div>
		</div>
	</div> -->
	<div class="top-nav clearfix">
		<div class="row">
			<div class="column">
				<div class="top-courselist">
					<?php 
						$defaults = array( 
								'menu' => 'top-menu',
								'container' 			=> '', 
								'container_class' => '',
								'menu_class'      => '',
								'menu_id'         => '',
								'items_wrap'      => '<ul class="float-left">%3$s</ul>',
							);
						wp_nav_menu($defaults); 
					?>
				</div>
				<div class="right-element float-right">
					<div class="tel"><?php the_field('header_tel', 'option'); ?></div>
					<!-- <div class="language"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/top-nav/thai.png"></a><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/top-nav/eng.png"></a></div> -->
				</div>
			</div>
		</div>
	</div>
	<nav class="clearfix">
		<div class="title-bar hide-for-large">
			<div class="logo-bar float-left"> 
        <a href="<?php echo site_url(); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/img/nav/chula-mobile-logo.png">
        </a>
      </div>
			<button class="menu-icon float-right" type="button"></button>
		</div>
		<div class="clearfix" id="main-menu">
      <div class="row search--container--top hidden">
        <div class="columns medium-12">

          <form role="search" method="get" id="searchform" class="search--container__top" action="<?php echo home_url( '/search' ); ?>">
            <input type="text" name="q" id="s" value="<?php echo get_search_query(); ?>" placeholder="ค้นหาคอร์สเรียน">
            <!-- <input type="hidden" name="filter" id="filter" value="<?php echo esc_attr($_GET["filter"]); ?>"> -->
            <a href="#" class="close--search"><i class="icon-close"></i></a>
          </form>

        </div>
      </div>
			<div class="row menu--container">
				<div class="logo-main float-left">
          <a href="<?php echo site_url(); ?>">
           <img class="show-for-large" src="<?php echo get_template_directory_uri(); ?>/img/nav/chula-desktop-logo.png">
         </a>
				</div>
      	<?php 
      		$defaults = array( 
      				'menu' => 'primary',
      				'container' 			=> '', 
      				'container_class' => '',
      				'menu_class'      => '',
      				'menu_id'         => '',
      				'items_wrap'      => '<ul class="dropdown menu main-menu float-left">%3$s</ul>',
      			);
      		wp_nav_menu($defaults); 
      	?>				
			</div>
		</div>
	</nav>
	<div class="news-bar">
		<div class="row">
			<div class="large-10 large-centered column">
				<p class="show-for-large"><?php the_field('header_news', 'option'); ?></p>
			</div>
		</div>
	</div>
  <?php  
    $fb = get_field('facebook', 'option');
    $tt = get_field('twitter', 'option');
    $ig = get_field('instagram', 'option');
    $ln = get_field('line', 'option');
  ?>
	<div class="social-float hide-for-small-only">
    <?php
      if ( $fb ):
    ?>
		<a class="fb" href="<?php echo $fb; ?>" target="_blank"></a>
    <?php
      endif;
      if ( $tt ):
    ?>
		<a class="tw" href="<?php echo $tt; ?>" target="_blank"></a>
    <?php
      endif;
      if ( $ig ):
    ?>
		<a class="ig" href="<?php echo $ig; ?>" target="_blank"></a>
    <?php
      endif;
      if ( $ln ):
    ?>
		<a class="ln" href="<?php echo $ln; ?>" target="_blank"></a>
    <?php
      endif;
    ?>
	</div>
<?php echo do_shortcode('[prisna-google-website-translator]'); ?>