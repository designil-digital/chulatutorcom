<?php 
  get_header(); 
?>
<?php the_field('top_banner'); ?>
<div class="hero">
  <div class="row expanded">
    <?php /*
      <img src="<?php the_field('top_banner', 'option'); ?>">
    */ ?>
    <?php /* Desktop */echo do_shortcode('[layerslider id="1"]'); ?>
    <?php /* Mobile */ echo do_shortcode('[layerslider id="2"]'); ?>
    <a class="call show-for-small-only" href="tel:<?php the_field('footer_tel', 'option'); ?>"></a>
    <div class="notification">
      <span>
        <?php 
          $news = get_field('update_news', 'option'); 
          echo count($news);
        ?>
      </span>
      <div class="noti-box hide">
        <div class="title">ข่าวอัพเดท</div>
        <div class="item">
          <ul>
            <?php
              if ( $news ) :
                foreach ($news as $key => $val) :
            ?>
            <li>
              <a href="<?php echo ($val['link'] == '') ? '#' : $val['link']; ?>" target="_blank" style="display: block; overflow: hidden;"> 
                <img src="<?php echo $val["image"]; ?>">
                <div class="detail">
                  <div class="title"><?php echo $val["title"]; ?></div>
                  <div class="desc"><?php echo $val["description"]; ?></div>
                </div>
              </a>
            </li>
            <?php              
                endforeach;
              endif;
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="search-box clearfix">
  <div class="brand medium-12 large-6 column">
    <div class="lead-img small-3 large-5 column">
      <img src="<?php echo get_template_directory_uri(); ?>/img/brand/brand.png">
    </div>
    <div class="small-9 large-7 column">
      <p><?php the_field('trophy_title', 'option'); ?></p><span><?php the_field('trophy_by', 'option'); ?></span>
    </div>
  </div>
  <div class="search--form large-6 show-for-large column">

    <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
      <div class="large-6 column">
        <input type="text" name="s" id="s" placeholder="ค้นหาคอร์สเรียน">
        <input type="hidden" name="filter" id="filter" value="course">
      </div>
      <div class="large-6 column">
        <button>ค้นหาคอร์ส</button>
      </div>
    </form>
    
  </div>
</div>
<div class="slide-home">
  <div class="row expanded">
    <div class="slide-group">    
      <?php 
        $quote = get_field('quote_slider', 'option'); 
        if ( $quote ) :
          foreach ($quote as $key => $val) :
      ?>
      <div class="item">
        <div class="medium-6 large-7 column img" style="background-image: url(<?php echo $val["image"]; ?>);"><img src="<?php echo get_template_directory_uri(); ?>/img/slider/gradient.png"></div>
        <div class="medium-6 large-5 column">
          <div class="quote small-7 small-offset-1 medium-8 medium-offset-2 large-7 large-offset-0 column">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/slider/logo.jpg">
            <p class="say"><?php echo $val["quote"]; ?></p>
            <p class="name"><?php echo $val["name"]; ?></p>
          </div>
        </div>
      </div>
      <?php
          endforeach;
        endif;
      ?>
    </div>
  </div>
</div>
<div class="channel">
  <div class="row">
    <div class="column">
      <div class="sub-head">ชาแนลของจุฬาติวเตอร์</div>
    </div>
    <!-- <div class="show-for-small-only">
      <div class="column"><img src="<?php echo get_template_directory_uri(); ?>/img/channel/ch-place-mobile.png"></div>
    </div> -->
    </div>
    <div class="for-button medium-1 column"></div>
    <div class="medium-10 column">
      <div class="slide-group row ">
        <?php 
          $channel = get_field('channel_slider', 'option'); 
          if ( $channel ) :
            foreach ($channel as $key => $val) :
        ?>
        <div class="item medium-6 large-4 column">
          <div class="hover"></div><a class="youtube-popup" href="<?php echo $val["link"]; ?>"><img src="<?php echo $val["image"]; ?>"></a>
        </div>
      <?php
          endforeach;
        endif;
      ?>
      </div>
    </div>
    <div class="for-button medium-1 column show-for-medium"></div>
  </div>
</div>
<div class="promote">
  <div class="row">
    <div class="column">
      <div class="sub-head">สถาบันยอดนิยม โดย</h3>
    </div>
  </div>
  <div class="row">
    <div class="medium-11 medium-centered column">
      <div class="row">
        <div class="for-button column medium-1 hide-for-small-only"></div>
        <div class="slide-group medium-10 column">        
        <?php 
          $institution = get_field('popular_institution', 'option'); 
          if ( $institution ) :
            foreach ($institution as $key => $val) :
        ?>
        <img src="<?php echo $val["url"]; ?>">
        <?php
            endforeach;
          endif;
        ?>            
        </div>
        <div class="for-button column medium-1 hide-for-small-only"></div>
      </div>
    </div>
  </div>
</div>

<?php 
  get_footer(); 
?>