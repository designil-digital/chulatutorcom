<?php 
get_header();
if ( have_posts() ):
  while ( have_posts() ): the_post();
?>
<div class="tutor-person">
  <div class="breadcrumb">
    <div class="row">
      <div class="small-12 column">
        <ul>
          <li>TUTOR</li>
          <li>ติวเตอร์ เปิ้ล</li>
        </ul>
        <h3>ติวเตอร์ เปิ้ล</h3>
      </div>
    </div>
  </div>
  <div class="row expanded">
    <div class="medium-4 column no-margin expanded">
      <div class="photo small-12" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>"></div>
      <div class="course small-12">
        <div class="large-7 large-offset-4">
          <div class="name">
            <p>อาจารย์ <?php the_field('bio_name'); ?></p>
            <p>ชื่อเล่น <?php the_field('bio_nickname'); ?></p>
          </div>
          <div class="open-course">
            <p>คอร์สที่สอน</p>
            <ul>
              <?php        
              $course = get_field('course');  
              global $post;
              if ( $course ) :
                foreach ($course as $key => $post) :
                  setup_postdata($post);
                  ?> 
              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
              <?php
                endforeach;
              endif;

              wp_reset_query();
              ?>              
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="medium-8 column no-margin">
      <div class="detail small-12" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/tutor/detail-bg.jpg');">
        <div class="detail-table">
          <div class="detail-table-cell">
            <div class="large-8 column">
              <div class="title"><strong><?php the_field('introduce_name'); ?></strong></div>
              <div class="desc"><?php the_field('introduce_short'); ?></div>
            </div>
          </div>
        </div>
      </div>
      <div class="photo-2 show-for-medium" style="background-image: url('<?php the_field('bio_image'); ?>');"></div>
    </div>
  </div>
  <div class="main-deatil">
    <div class="row">
      <div class="small-12 column">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<?php
  endwhile;
endif;
get_footer();
?>