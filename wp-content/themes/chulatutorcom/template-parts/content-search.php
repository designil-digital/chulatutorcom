<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package chulatutorcom
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<figure>
		<?php
		if ( has_post_thumbnail() ) {
			$url = get_the_post_thumbnail_url();
		} else {
			$url = 'http://placehold.it/80x80';
		}
		?>
		<div class="search--thumb" style="background-image:url('<?php echo $url; ?>');"></div>
	</figure>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
</article><!-- #post-## -->
