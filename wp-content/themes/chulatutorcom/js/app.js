(function($) {

  $(document).ready(function(){

    $('.top-courselist').css({ 'height': '3em', 'overflow': 'scroll'});

    var sumwidth = 0;
    $('.top-courselist').find('> ul > li').each(function() {
      $(this).css({'display': 'block', 'float': 'left'});
      sumwidth += $(this)[0].getBoundingClientRect().width + parseInt( $(this).css('margin-right') );
    });
    $('.top-courselist').find('> ul').css('height', '3em').css('width', sumwidth);
    

    $('.slide-home .slide-group').slick({
      dots: true,
      infinite: true,
      speed: 500,
      touchMove: true,
      autoplay: true,
      autoplaySpeed: 5000,
      pauseOnHover: false,
    });

    $('.channel .slide-group').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      rows: 2,
      dots: true,
      responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          rows: 2,
        }
      }
      ]
    });

    $('.promote .slide-group').slick({
     infinite: true,
     slidesToShow: 6,
     slidesToScroll: 6,
     dots: true,
   });

    $('.course-slide .slide-group').slick({
      dots: true,
      infinite: true,
      speed: 500,

      touchMove: true
    });

    $('.portfolio.video .slide-group').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive: [
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,

        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
      ]
    });

    // Home Video Popup
    $('.youtube-popup').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false
    }); 

    $('.go-to-top').click(function(){
      $('html, body').animate({
        scrollTop: 0
      }, 500);

      return false;
    });

    if ( $('body.search').length > 0 ) {
      $('.logo-main img').toggleClass('hidden');
      $('.search--container--top').toggleClass('hidden');
    }

    $('.close--search, .search--bar').click(function(){
      $('.logo-main img').toggleClass('hidden');
      $('.search--container--top').toggleClass('hidden');
      $('#s').select();
    });

    $('.search--bar').click(function(){
      if ( $('.search--bar').is(':visible') ) {
        $('ul.dropdown').slideUp();
      }
    });

  });


  $(document).ready(function(){
    $("button.menu-icon").click(function(){
      $("ul.dropdown").slideToggle( "hidden" );
    });

    // Menu    
    $(".sub-menu").wrap('<div class="sub-menu-item"></div>');
    $(".sub-menu").wrap('<div class="block-contain clearfix"></div>');
    $(".menu-item-has-children").each(function(){
      $(this).addClass('sub-menu');
      var title = $(this).find('a').attr('title');
      $(this).find('.sub-menu').before('<h2 class="float-left show-for-large">'+title+'</h2>');    
      $(this).find('.sub-menu').addClass('float-left');  
      $(this).find('.sub-menu').removeClass('sub-menu');
    });

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $("li.sub-menu").click(function(){
        if ( $(this).hasClass('active') ) {
          $(this).removeClass('active');
          $(this).removeClass('rotate-arrow');
          $(".sub-menu-item").slideUp("hidden");
        } else {
          $('li.sub-menu').removeClass('active');
          $('li.sub-menu').removeClass('rotate-arrow');
          $(this).addClass('active');
          $(this).addClass('rotate-arrow');
          $(".sub-menu-item").slideUp("hidden");
          $(".sub-menu-item",this).slideDown("hidden");
        }
      });
    } else {
      $("li.sub-menu").mouseenter(function() {
        $('li.sub-menu').removeClass('active');
        $('li.sub-menu').removeClass('rotate-arrow');
        $(this).addClass('active');
        $(this).addClass('rotate-arrow');
        $(".sub-menu-item").fadeOut("fast");
        $(".sub-menu-item",this).fadeIn("fast");
      }).mouseleave(function() {
        $(this).removeClass('active');
        $(this).removeClass('rotate-arrow');
        $(".sub-menu-item").fadeOut("fast");
      });
    }

    $(".read-more a").click(function(){
      $(".about .content").toggleClass("fix-height");
      if($(".about .content").hasClass("fix-height")){
        $(".read-more").removeClass("open");
      } else {
        $(".read-more").addClass("open");
      }
    });    

    $('.share-btn a').click(function(){
      url = encodeURIComponent(window.location.href);
      title = 'Social Popup';
      image = '';
      descr = '';
      winHeight = 350;
      winWidth = 520;   

      var winTop = (screen.height / 2) - (winHeight / 2);
      var winLeft = (screen.width / 2) - (winWidth / 2);

      switch( $(this).attr("class") ){
        case 'fb':
        url = 'https://www.facebook.com/sharer.php?u=' + url;
        break;
        case 'tw':
        url = 'https://twitter.com/intent/tweet?original_referer=' + url + '&text=&url=' + url;
        break;
      }

      window.open( url , 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);    

      return false;
    });
  });

  $(document).ready(function(){
    $(".course-detail .tabbar-content .ps").addClass("hide");
    $(".tabbar .tab-list a").click(function(){
      event.preventDefault();
      $(".tabbar .tab-list a").removeClass("active");
      $(this).addClass("active");

      if($(".course-detail .tabbar .tab-list a").hasClass("detail active")){
        $(".course-detail .tabbar-content .detail").removeClass("hide");
        $(".course-detail .tabbar-content .ps").addClass("hide");
      } else{
        $(".course-detail .tabbar-content .ps").removeClass("hide");
        $(".course-detail .tabbar-content .detail").addClass("hide");
      }

    });

    $(".course-detail a.read-more").click(function(){
      event.preventDefault();
      $(".course-detail .tabbar-content .content.detail").toggleClass("fix-height");
      if($(".course-detail .tabbar-content .content.detail").hasClass("fix-height")){
        $(".read-more").removeClass("open");
      } else {
        $(".read-more").addClass("open");
      }
    });

    $(".notification").click(function(){
      $(".notification .noti-box").toggleClass("hide");
    });
  });  

})(jQuery);






