<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package chulatutorcom
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function chulatutorcom_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'chulatutorcom_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function chulatutorcom_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'chulatutorcom_pingback_header' );

/* Add new page to ACF */
if( function_exists('acf_add_options_page') ) {
	
	// add parent
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title' 	=> 'Theme Settings',
		'redirect' 		=> false
	));
	
	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Home Page Settings',
		'menu_title' 	=> 'Home Page',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Portolio Page Settings',
		'menu_title' 	=> 'Portolio Page',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'About Us Page Settings',
		'menu_title' 	=> 'About Us Page',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Contact Us Page Settings',
		'menu_title' 	=> 'Contact Us Page',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Tutor Register Page Settings',
		'menu_title' 	=> 'Tutor Register Page',
		'parent_slug' 	=> $parent['menu_slug'],
	));
}

function register_chulatutor_menu() {
  register_nav_menu('top-menu',__( 'Top Menu' ));
  register_nav_menu('footer-menu-1',__( 'Footer Menu - 1' ));
  register_nav_menu('footer-menu-2',__( 'Footer Menu - 2' ));
  register_nav_menu('footer-menu-3',__( 'Footer Menu - 3' ));
}
add_action( 'init', 'register_chulatutor_menu' );


/* Share Count Social */
function get_facebook_count( $url ) {
  // Get Facebook API from Transient
  if ( false === ( $fbcount = get_transient( $url ) ) ) {
    // It wasn't there, regenerate the data and save the transient
    $this_url = rawurlencode( $url );
    $json_string = @file_get_contents('http://graph.facebook.com/?id=' . $this_url);
    $json = json_decode($json_string, true);
    $fbcount = isset($json['share']['share_count']) ? intval($json['share']['share_count']) : 0;
    set_transient( 'fbshare_' . $this_url, $fbcount, HOUR_IN_SECONDS );
  }
  return $fbcount;
}

/* For no landing page only 1 level */
function chulatutor_template_redirect()
{ 
  if (is_singular('course')) {
    $no_landing_page = get_field('course_redirect', get_the_ID());

    if (!empty($no_landing_page)){

      if ($no_landing_page[0] == "true") {
        $url = get_field('course_redirect_to', get_the_ID());
        if (!empty($url)){
          wp_redirect( $url );
          exit();
        }
      }

    }
  }
}
add_action( 'template_redirect', 'chulatutor_template_redirect' );

/* Post per page */
function post_filter($query) {
  if (  $query->is_main_query() && !is_admin() ) {

    if ( $query->is_search && $_GET["filter"] == 'course' ) {
      $query->set('post_type', 'course');
    }

  }
}
add_action('pre_get_posts','post_filter');